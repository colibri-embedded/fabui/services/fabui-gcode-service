#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>

#include <gcode_file.hpp>
#include <string>
#include <iostream>

#define SLIC3R_DIR DATA_DIR"/gcode/print/prusa3d/slic3r"
#define GENERIC_DIR DATA_DIR"/gcode/print/generic"

using namespace fabui;

SCENARIO("Open file", "[gcode-file][open-close]") {

	WHEN("No filename is provided") {
		GCodeFile gf;

		THEN("File is not loaded") {
			REQUIRE_FALSE( gf.isOpened() );
			REQUIRE_FALSE( gf.eof() );
			REQUIRE( gf.getCurrentLineNumber() == 0  );
			REQUIRE( gf.getNumberOfLines() == 0  );
			REQUIRE( gf.getProgress() == 0.0f  );
		}
	}

	WHEN("Invalid filename is provided") {
		GCodeFile gf(SLIC3R_DIR"/PLA_tr_1.0-v2.1.gcode_xyz");

		THEN("File is not loaded") {
			REQUIRE_FALSE( gf.isOpened() );
			REQUIRE_FALSE( gf.eof() );
			REQUIRE( gf.getCurrentLineNumber() == 0  );
			REQUIRE( gf.getNumberOfLines() == 0  );
			REQUIRE( gf.getProgress() == 0.0f  );
		}
	}

	WHEN("Valid filename is provided") {
		GCodeFile gf(SLIC3R_DIR"/PLA_tr_1.0-v2.1.gcode");

		THEN("File is loaded") {
			REQUIRE( gf.isOpened() );
			REQUIRE_FALSE( gf.eof() );
			REQUIRE( gf.getCurrentLineNumber() == 0  );
			REQUIRE( gf.getNumberOfLines() == 280015  );
			REQUIRE( gf.getProgress() == 0.0f  );
		}
	}

	WHEN("Open is called with a invalid filename") {
		GCodeFile gf;

		THEN("File is not loaded") {
			REQUIRE_FALSE( gf.open(SLIC3R_DIR"/PLA_tr_1.0-v2.1.gcode_xyz") );
			REQUIRE_FALSE( gf.isOpened() );
			REQUIRE_FALSE( gf.eof() );
			REQUIRE( gf.getCurrentLineNumber() == 0  );
			REQUIRE( gf.getNumberOfLines() == 0  );
			REQUIRE( gf.getProgress() == 0.0f  );
		}
	}

	WHEN("Open is called with a valid filename") {
		GCodeFile gf;

		THEN("File is loaded") {
			REQUIRE( gf.open(SLIC3R_DIR"/PLA_tr_1.0-v2.1.gcode") );

			REQUIRE( gf.isOpened() );
			REQUIRE_FALSE( gf.eof() );
			REQUIRE( gf.getCurrentLineNumber() == 0  );
			REQUIRE( gf.getNumberOfLines() == 280015  );
			REQUIRE( gf.getProgress() == 0.0f  );
		}
	}

	WHEN("Close is called on an opened file") {
		GCodeFile gf;
		REQUIRE( gf.open(SLIC3R_DIR"/PLA_tr_1.0-v2.1.gcode") );

		THEN("File is loaded") {
			REQUIRE_NOTHROW( gf.close() );

			REQUIRE_FALSE( gf.isOpened() );
			REQUIRE_FALSE( gf.eof() );
			REQUIRE( gf.getCurrentLineNumber() == 0  );
			REQUIRE( gf.getNumberOfLines() == 0  );
			REQUIRE( gf.getProgress() == 0.0f  );
		}
	}

}

SCENARIO("Read lines", "[gcode-file][read]") {

	WHEN("File wihtout final EOL is provided") {
		GCodeFile gf(GENERIC_DIR"/no_final_eol.gcode");
		REQUIRE( gf.isOpened() );

		THEN("Lines can be read") {
			REQUIRE( gf.getNumberOfLines() == 10  );

			while( not gf.eof() ) {
				std::string line, comment;
				REQUIRE( gf.nextLine(line, comment) );
			}

			REQUIRE( gf.getCurrentLineNumber() == gf.getNumberOfLines()  );
		}
	}

	WHEN("Valid file is provided") {
		GCodeFile gf(GENERIC_DIR"/10lines.gcode");
		REQUIRE( gf.isOpened() );

		THEN("Lines can be read") {
			REQUIRE( gf.getNumberOfLines() == 11  );

			while( not gf.eof() ) {
				std::string line, comment;
				REQUIRE( gf.nextLine(line, comment) );
			}

			REQUIRE( gf.getCurrentLineNumber() == gf.getNumberOfLines()  );
		}
	}

	WHEN("Valid file is provided") {
		GCodeFile gf(GENERIC_DIR"/no_final_eol.gcode");
		REQUIRE( gf.isOpened() );

		THEN("Line numbers are correct") {
			REQUIRE( gf.getNumberOfLines() == 10  );

			unsigned i=0;
			while( not gf.eof() ) {
				std::string line, comment;
				REQUIRE( gf.nextLine(line, comment) );

				REQUIRE( ++i == gf.getCurrentLineNumber() );
			}

			REQUIRE( gf.getCurrentLineNumber() == gf.getNumberOfLines()  );
		}
	}	

	WHEN("Valid file is provided") {
		GCodeFile gf(GENERIC_DIR"/no_final_eol.gcode");
		REQUIRE( gf.isOpened() );

		THEN("Progress is updated correctly") {
			REQUIRE( gf.getNumberOfLines() == 10  );

			unsigned i=0;
			while( not gf.eof() ) {
				std::string line, comment;
				REQUIRE( gf.nextLine(line, comment) );

				REQUIRE( int(++i*10.0f) == (int)gf.getProgress() );
			}

			REQUIRE( gf.getCurrentLineNumber() == gf.getNumberOfLines()  );
		}
	}

	WHEN("Valid file is provided") {
		GCodeFile gf(GENERIC_DIR"/no_final_eol.gcode");
		REQUIRE( gf.isOpened() );

		THEN("GCode and comments are extracted correctly") {
			REQUIRE( gf.getNumberOfLines() == 10  );

			std::string code, comment;
			// M107
			REQUIRE( gf.nextLine(code, comment) );
			REQUIRE( code == "M107");
			REQUIRE( comment == "");
			// M115 U3.0.9 ; tell printer latest fw version
			REQUIRE( gf.nextLine(code, comment) );
			REQUIRE( code == "M115 U3.0.9");
			REQUIRE( comment == "tell printer latest fw version");
			// M83  ; extruder relative mode
			REQUIRE( gf.nextLine(code, comment) );
			REQUIRE( code == "M83");
			REQUIRE( comment == "extruder relative mode");
			// M104 S210 ; set extruder temp
			REQUIRE( gf.nextLine(code, comment) );
			REQUIRE( code == "M104 S210");
			REQUIRE( comment == "set extruder temp");
			// M140 S65 ; set bed temp
			REQUIRE( gf.nextLine(code, comment) );
			REQUIRE( code == "M140 S65");
			REQUIRE( comment == "set bed temp");
			// M190 S65 ; wait for bed temp
			REQUIRE( gf.nextLine(code, comment) );
			REQUIRE( code == "M190 S65");
			REQUIRE( comment == "wait for bed temp");
			// M109 S210 ; wait for extruder temp
			REQUIRE( gf.nextLine(code, comment) );
			REQUIRE( code == "M109 S210");
			REQUIRE( comment == "wait for extruder temp");
			// G28 W ; home all without mesh bed level
			REQUIRE( gf.nextLine(code, comment) );
			REQUIRE( code == "G28 W");
			REQUIRE( comment == "home all without mesh bed level");
			// G80 ; mesh bed leveling
			REQUIRE( gf.nextLine(code, comment) );
			REQUIRE( code == "G80");
			REQUIRE( comment == "mesh bed leveling");
			// G1 Y-3.0 F1000.0 ; go outside pritn area
			REQUIRE( gf.nextLine(code, comment) );
			REQUIRE( code == "G1 Y-3.0 F1000.0");
			REQUIRE( comment == "go outside pritn area");

		}
	}	


}
