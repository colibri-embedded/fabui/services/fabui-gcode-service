#include <iostream>

#include <functional>
#include <dlfcn.h>
#include "command.hpp"
#include "plugins/gcode/base.hpp"

using namespace fabui;
using namespace fabui::command;

plugin::gcode::Plugin* loadGcodePlugin(const std::string& filename) {
	void *hndl = dlopen(filename.c_str(), RTLD_NOW);

	if(hndl == nullptr) {
		std::cerr << dlerror() << std::endl;
		return nullptr;
	}

	void *mkr = dlsym(hndl, "create");
	if(mkr == nullptr) {
		std::cerr << dlerror() << std::endl;
		return nullptr;
	}

	auto create = (plugin::gcode::create_fn_t)mkr;
	return create(nullptr);
}

int main() {

	/*void *hndl = dlopen("./src/plugins/gcode/libmarlin-1.1.x.so", RTLD_NOW);

	if(hndl == nullptr) {
		std::cerr << dlerror() << std::endl;
		exit(-1);
	}

	void *mkr = dlsym(hndl, "create");
	auto create = (plugin::gcode::create_fn_t)mkr;
	auto plugin = create(nullptr);*/

	auto plugin = loadGcodePlugin("./src/plugins/gcode/libmarlin-1.1.x.so");

	std::cout << "name: " << plugin->name() << std::endl;


	GCode m190("M190 S100");
	std::string reply = "T:29.47 E:0 B:28.7";
	auto extra = plugin->parseReplyLine(&m190, reply);
	std::cout << "M190: " << reply << std::endl;
	std::cout << "      " << extra << std::endl;

	GCode m109("M109 S100");
	reply = "T:217.2 E:0 W:?";
	extra = plugin->parseReplyLine(&m109, reply);
	std::cout << "M109: " << reply << std::endl;
	std::cout << "      " << extra << std::endl;

	GCode m105("M105");
	reply = "ok T:31.4 /0.0 B:30.7 /0.0 T0:31.4 /0.0 @:0 B@:0";
	extra = plugin->parseReplyLine(&m105, reply);
	std::cout << "M105: " << reply << std::endl;
	std::cout << "      " << extra << std::endl;

	return 0;
}