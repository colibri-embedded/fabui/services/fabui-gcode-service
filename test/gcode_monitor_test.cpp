#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>

#include <gcode_parser.hpp>
#include <gcode_monitor.hpp>
#include <gcode_modifier.hpp>

using namespace fabui;

SCENARIO("Movement commands", "[gcode-monitor]") {

	GCodeParser gp;
	std::string comment;

	GIVEN("New monitor object") {
		GCodeState   s;
		GCodeModifier gm(s);
		GCodeMonitor gmon(s, gm);

		WHEN("G0 code is sent") {
			auto g = gp.parse("G0 X1 Y2 Z3 F1500");

			REQUIRE( s.z == 0.0f );
			REQUIRE( s.feedrate == 0 );
			gmon.process(g, comment);

			THEN("Z and F values are stored") {
				REQUIRE( s.z == 3.0f );
				REQUIRE( s.feedrate == 1500 );
			}
		}

		WHEN("G1 code is sent") {
			auto g = gp.parse("G1 X1 Y2 Z3 F1500");

			REQUIRE( s.z == 0.0f );
			REQUIRE( s.feedrate == 0 );
			gmon.process(g, comment);

			THEN("Z and F values are stored") {
				REQUIRE( s.z == 3.0f );
				REQUIRE( s.feedrate == 1500 );
			}
		}


		WHEN("G2 code is sent") {
			auto g = gp.parse("G2 X1 Y2 Z3 F1500");

			REQUIRE( s.z == 0.0f );
			REQUIRE( s.feedrate == 0 );
			gmon.process(g, comment);

			THEN("Z and F values are stored") {
				REQUIRE( s.z == 3.0f );
				REQUIRE( s.feedrate == 1500 );
			}
		}


		WHEN("G3 code is sent") {
			auto g = gp.parse("G3 X1 Y2 Z3 F1500");

			REQUIRE( s.z == 0.0f );
			REQUIRE( s.feedrate == 0 );
			gmon.process(g, comment);

			THEN("Z and F values are stored") {
				REQUIRE( s.z == 3.0f );
				REQUIRE( s.feedrate == 1500 );
			}
		}

		WHEN("G90 code is sent") {
			auto g = gp.parse("G90");
			s.xyz_mode = gcode::Positioning::RELATIVE;
			gmon.process(g, comment);

			THEN("xyz mode is set to ABSOLUTE") {
				REQUIRE( s.xyz_mode == gcode::Positioning::ABSOLUTE );
			}
		}

		WHEN("G91 code is sent") {
			auto g = gp.parse("G91");
			s.xyz_mode = gcode::Positioning::ABSOLUTE;
			gmon.process(g, comment);

			THEN("xyz mode is set to ABSOLUTE") {
				REQUIRE( s.xyz_mode == gcode::Positioning::RELATIVE );
			}
		}

		WHEN("M82 code is sent") {
			auto g = gp.parse("M82");
			s.e_mode = gcode::Positioning::RELATIVE;
			gmon.process(g, comment);

			THEN("xyz mode is set to ABSOLUTE") {
				REQUIRE( s.e_mode == gcode::Positioning::ABSOLUTE );
			}
		}

		WHEN("M83 code is sent") {
			auto g = gp.parse("M83");
			s.e_mode = gcode::Positioning::ABSOLUTE;
			gmon.process(g, comment);

			THEN("xyz mode is set to ABSOLUTE") {
				REQUIRE( s.e_mode == gcode::Positioning::RELATIVE );
			}
		}

	}

}

SCENARIO("Spindle commands", "[gcode-monitor]") {

	GCodeParser gp;
	std::string comment;

	GIVEN("New monitor object") {
		GCodeState   s;
		GCodeModifier gm(s);
		GCodeMonitor gmon(s, gm);

		WHEN("M3 code without argument is sent") {
			auto g = gp.parse("M3");
			gmon.process(g, comment);

			THEN("Nothing changes") {
				REQUIRE( s.rpm == 0);
				REQUIRE( s.rotation == gcode::Rotation::CW );
			}
		}

		WHEN("M3 code with argument is sent") {
			auto g = gp.parse("M3 S14000");
			gmon.process(g, comment);

			THEN("Spindle state is updated") {
				REQUIRE( s.rpm == 14000);
				REQUIRE( s.rotation == gcode::Rotation::CW );
			}
		}

		WHEN("M4 code without argument is sent") {
			auto g = gp.parse("M4");
			gmon.process(g, comment);

			THEN("Nothing changes") {
				REQUIRE( s.rpm == 0);
				REQUIRE( s.rotation == gcode::Rotation::CW );
			}
		}

		WHEN("M4 code with argument is sent") {
			auto g = gp.parse("M4 S14000");
			gmon.process(g, comment);

			THEN("Spindle state is updated") {
				REQUIRE( s.rpm == 14000);
				REQUIRE( s.rotation == gcode::Rotation::CCW );
			}
		}

		WHEN("M5 code sent") {
			auto g = gp.parse("M5");
			s.rpm = 14000;
			gmon.process(g, comment);

			THEN("Spindle is stopped") {
				REQUIRE( s.rpm == 0);
			}
		}

	}
}

SCENARIO("Temperature commands", "[gcode-monitor]") {

	GCodeParser gp;
	std::string comment;

	GIVEN("New monitor object") {
		GCodeState   s;
		GCodeModifier gm(s);
		GCodeMonitor gmon(s, gm);

		WHEN("M140 without arguments is sent") {
			auto g = gp.parse("M140");
			gmon.process(g, comment);

			THEN("Nothing changes") {
				REQUIRE( s.nozzle_target[0] == 0);
				REQUIRE( s.nozzle_target[1] == 0);
				REQUIRE( s.nozzle_target[2] == 0);
				REQUIRE( s.nozzle_target[3] == 0);
				REQUIRE( s.nozzle_target[4] == 0);
			}
		}

		WHEN("M140 with temp argument is sent") {
			auto g = gp.parse("M140 S200");
			gmon.process(g, comment);

			THEN("Value is update") {
				REQUIRE( s.nozzle_target[0] == 200);
				REQUIRE( s.nozzle_target[1] == 0);
				REQUIRE( s.nozzle_target[2] == 0);
				REQUIRE( s.nozzle_target[3] == 0);
				REQUIRE( s.nozzle_target[4] == 0);
			}
		}

		WHEN("M140 with temp and index arguments is sent") {
			auto g = gp.parse("M140 S200 T2");
			gmon.process(g, comment);

			THEN("Value is update") {
				REQUIRE( s.nozzle_target[0] == 0);
				REQUIRE( s.nozzle_target[1] == 0);
				REQUIRE( s.nozzle_target[2] == 200);
				REQUIRE( s.nozzle_target[3] == 0);
				REQUIRE( s.nozzle_target[4] == 0);
			}
		}

		WHEN("M104 without arguments is sent") {
			auto g = gp.parse("M104");
			gmon.process(g, comment);

			THEN("Nothing changes") {
				REQUIRE( s.nozzle_target[0] == 0);
				REQUIRE( s.nozzle_target[1] == 0);
				REQUIRE( s.nozzle_target[2] == 0);
				REQUIRE( s.nozzle_target[3] == 0);
				REQUIRE( s.nozzle_target[4] == 0);
			}
		}

		WHEN("M104 with temp argument is sent") {
			auto g = gp.parse("M104 S200");
			gmon.process(g, comment);

			THEN("Value is update") {
				REQUIRE( s.nozzle_target[0] == 200);
				REQUIRE( s.nozzle_target[1] == 0);
				REQUIRE( s.nozzle_target[2] == 0);
				REQUIRE( s.nozzle_target[3] == 0);
				REQUIRE( s.nozzle_target[4] == 0);
			}
		}

		WHEN("M104 with temp and index arguments is sent") {
			auto g = gp.parse("M104 S200 T2");
			gmon.process(g, comment);

			THEN("Value is update") {
				REQUIRE( s.nozzle_target[0] == 0);
				REQUIRE( s.nozzle_target[1] == 0);
				REQUIRE( s.nozzle_target[2] == 200);
				REQUIRE( s.nozzle_target[3] == 0);
				REQUIRE( s.nozzle_target[4] == 0);
			}
		}

		WHEN("M190 without arguments is sent") {
			auto g = gp.parse("M190");
			gmon.process(g, comment);

			THEN("Nothing changes") {
				REQUIRE( s.bed_target == 0);
			}
		}

		WHEN("M190 with temp argument is sent") {
			auto g = gp.parse("M190 S100");
			gmon.process(g, comment);

			THEN("Value is update") {
				REQUIRE( s.bed_target == 100);
			}
		}

		WHEN("M109 without arguments is sent") {
			auto g = gp.parse("M109");
			gmon.process(g, comment);

			THEN("Nothing changes") {
				REQUIRE( s.bed_target == 0);
			}
		}

		WHEN("M109 with temp argument is sent") {
			auto g = gp.parse("M109 S100");
			gmon.process(g, comment);

			THEN("Value is update") {
				REQUIRE( s.bed_target == 100);
			}
		}

		WHEN("M106 without arguments is sent") {
			auto g = gp.parse("M106");
			gmon.process(g, comment);

			THEN("Nothing changes") {
				REQUIRE( s.fan_pwm == 0);
			}
		}

		WHEN("M106 with argument is sent") {
			auto g = gp.parse("M106 S128");
			gmon.process(g, comment);

			THEN("Value is update") {
				REQUIRE( s.fan_pwm == 128);
			}
		}

		WHEN("M107 is sent") {
			auto g = gp.parse("M107");
			s.fan_pwm = 255;
			gmon.process(g, comment);

			THEN("Fan is off") {
				REQUIRE( s.fan_pwm == 0);
			}
		}

	}

}

SCENARIO("Speed & Flow commands", "[gcode-monitor]") {

	GCodeParser gp;
	std::string comment;

	GIVEN("New monitor object") {
		GCodeState   s;
		GCodeModifier gm(s);
		GCodeMonitor gmon(s, gm);

		WHEN("M220 without arguments is sent") {
			auto g = gp.parse("M220");
			gmon.process(g, comment);

			THEN("Nothing changes") {
				REQUIRE( s.speed == 100.0f);
			}
		}

		WHEN("M220 with argument is sent") {
			auto g = gp.parse("M220 S150");
			gmon.process(g, comment);

			THEN("Value is update") {
				REQUIRE( s.speed == 150.0f);
			}
		}

		WHEN("M221 without arguments is sent") {
			auto g = gp.parse("M221");
			gmon.process(g, comment);

			THEN("Nothing changes") {
				REQUIRE( s.flow == 100.0f);
			}
		}

		WHEN("M221 with argument is sent") {
			auto g = gp.parse("M221 S150");
			gmon.process(g, comment);

			THEN("Value is update") {
				REQUIRE( s.flow == 150.0f);
			}
		}

	}
}

// TODO: M117
// TODO: M240
// TODO: M401, M402