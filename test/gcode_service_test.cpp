#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include <gcode_service.hpp>
#include <topics.hpp>
#include "serial_bridge.hpp"
#include "machine/generic.hpp"

#include <future>
#include <vector>
#include <memory>
#include <map>
#include <fabui/wamp/mock/session.hpp>

using namespace fabui;
using namespace std::placeholders;
using namespace std::literals;

/****** Constants *******/
constexpr char UART_PORT[] = "/tmp/ttyDUT0";
constexpr char TEST_PORT[] = "/tmp/ttyDUT1";
constexpr unsigned UART_BAUDRATE = 115000;

#define TIC std::chrono::high_resolution_clock::duration tic_time = std::chrono::high_resolution_clock::now().time_since_epoch()
#define TOC std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()-tic_time).count()

#define SLIC3R_DIR DATA_DIR"/gcode/print/prusa3d/slic3r"
#define GENERIC_DIR DATA_DIR"/gcode/print/generic"

/******* Helpers *******/
auto bridge = test::SerialBridge::getInstance();

class GCodeServiceSpy: public GCodeService {
	public:
		GCodeServiceSpy(const std::string& portName = UART_PORT,
			unsigned baudRate=UART_BAUDRATE, 
			unsigned timeout=200) 
			: GCodeService(portName, baudRate, timeout) {

			}

		bool isSerialOpen() {
			return m_serial.isOpen();
		}

		bool isRunning() {
			std::lock_guard<std::mutex> guard(m_running_mut);
			return m_running_state == State::RUNNING;
		}

		bool isSuspended() {
			std::lock_guard<std::mutex> guard(m_running_mut);
			return m_running_state == State::SUSPENDED;
		}
};

class GenericMachineSpy: public test::GenericMachine {
	public:
		GenericMachineSpy(const std::string& portName, unsigned baudRate=115200, unsigned timeout=200) 
			: test::GenericMachine(portName, baudRate, timeout) { }

		bool isSerialOpen() {
			return m_serial.isOpen();
		}

		bool onReceive(const std::string& line) {
			// std::cout << line << std::endl;
			for(auto &resp: m_responders) {
				auto prefix = resp.first;
				if( line.compare(0, prefix.size(), prefix) == 0 ) {
					return resp.second(line);
				}
			}
			return false;
		}

		void addResponder(const std::string& prefix, std::function<bool(const std::string&)> callback) {
			m_responders[prefix] = callback;
		}

	private:
		std::map<std::string, std::function<bool(const std::string&)> > m_responders;
};

class Delayed {
	public:
		Delayed(unsigned delay, std::function<void()> callback) 
			: m_delay(delay)
			, m_callback(callback)
		{
			
		}

		~Delayed() {
			if(m_thread.joinable())
				m_thread.join();
		}

		void start() {
			m_thread = std::thread(&Delayed::loop, this);
		}

		void loop() {
			std::this_thread::sleep_for( std::chrono::milliseconds(m_delay)  );
			m_callback();
		}

		std::thread m_thread;

		unsigned m_delay;
		std::function<void()> m_callback;
};

/***** TESTS *****/

SCENARIO_METHOD(test::SerialBridge, "New object state transition", "[gcode-service][states]") {

	WHEN("New object is created -> destroy") {

		THEN("Object is destroyed successfully") {
			GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
		}

	}

	WHEN("Start is called on a new object") {

		THEN("Start is successfully called") {
			GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
			REQUIRE( gss.start() );
			REQUIRE( gss.isRunning() );
		}

	}

	// TODO: start is called on a started object

	WHEN("Stop is called on a not started object") {
		GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
		REQUIRE_FALSE( gss.isRunning() );

		THEN("Stop is successfully called after start") {
			REQUIRE_NOTHROW( gss.stop() );
			REQUIRE_FALSE( gss.isRunning() );
		}
	}

	WHEN("Stop is called on a started object") {
		GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
		REQUIRE( gss.start() );
		REQUIRE( gss.isRunning() );

		THEN("Stop is successfully called after start") {
			REQUIRE_NOTHROW( gss.stop() );
			REQUIRE_FALSE( gss.isRunning() );
		}
	}

	WHEN("Stop is called on a stopped object") {
		GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);

		REQUIRE( gss.start() );
		REQUIRE( gss.isRunning() );

		THEN("Stop is successfully called after start 2x") {
			REQUIRE_NOTHROW( gss.stop() );
			REQUIRE_FALSE( gss.isRunning() );

			REQUIRE_NOTHROW( gss.stop() );
			REQUIRE_FALSE( gss.isRunning() );
		}
	}

	WHEN("Suspend is called on a not started object") {
		GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
		REQUIRE_FALSE( gss.isRunning() );

		THEN("Object successfully destroyed later") {
			REQUIRE_FALSE( gss.suspend() );
			REQUIRE_FALSE( gss.isRunning() );
		}
	}

	WHEN("Suspend is called on a started object") {
		GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
		REQUIRE( gss.start() );
		REQUIRE( gss.isRunning() );

		THEN("Object successfully destroyed later") {
			REQUIRE( gss.suspend() );
			REQUIRE_FALSE( gss.isRunning() );
		}
	}

	WHEN("Resume is called on a not started object") {
		GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
		REQUIRE_FALSE( gss.isRunning() );

		THEN("Object successfully destroyed later") {
			REQUIRE_FALSE( gss.resume() );
			REQUIRE_FALSE( gss.isRunning() );
		}
	}

	WHEN("Resume is called on a started object") {
		GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
		REQUIRE( gss.start() );
		REQUIRE( gss.isRunning() );

		THEN("Object successfully destroyed later") {
			REQUIRE_FALSE( gss.resume() );
			REQUIRE( gss.isRunning() );
		}
	}

	WHEN("Resume is called on a suspended object") {
		GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
		REQUIRE( gss.start() );
		REQUIRE( gss.isRunning() );

		THEN("Object successfully destroyed later") {
			REQUIRE( gss.suspend() );
			REQUIRE_FALSE( gss.isRunning() );
		}
	}
}

SCENARIO_METHOD(test::SerialBridge, "Serial port control", "[gcode-service][serial]") {

	GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);

	WHEN("New object is created") {
		THEN("Serial is closed by default") {
			REQUIRE_FALSE( gss.isSerialOpen() );
		}
	}

	WHEN("New object is started") {
		REQUIRE_FALSE( gss.isSerialOpen() );

		THEN("Serial is opened") {
			REQUIRE( gss.start() );
			REQUIRE_NOTHROW( gss.isSerialOpen() );
			REQUIRE( gss.isSerialOpen() );
		}
	}

	WHEN("Stop is called") {
		REQUIRE( gss.start() );
		REQUIRE_NOTHROW( gss.stop() );

		THEN("Serial is closed") {
			REQUIRE_FALSE( gss.isSerialOpen() );
		}
	}

	WHEN("serialClose is called on a new object") {
		THEN("Serial port is not closed and false is returned") {
			REQUIRE_FALSE( gss.serialClose() );
			REQUIRE_FALSE( gss.isRunning() );
		}
	}

	WHEN("serialClose is called") {
		REQUIRE( gss.start() );
		REQUIRE( gss.isSerialOpen() );
		REQUIRE( gss.serialClose() );

		THEN("Serial is closed and service is supended") {
			REQUIRE_FALSE( gss.isSerialOpen() );
			REQUIRE( gss.isSuspended() );
			REQUIRE_FALSE( gss.isRunning() );
		}
	}

	WHEN("serialOpen is called on a new object") {
		THEN("Serial port is not opened and false is returned") {
			REQUIRE_FALSE( gss.serialOpen() );
			REQUIRE_FALSE( gss.isSerialOpen() );
		}
	}
	
	WHEN("serialOpen is called after serialClose") {
		REQUIRE( gss.start() );
		REQUIRE( gss.isSerialOpen() );
		REQUIRE( gss.serialClose() );
		REQUIRE_FALSE( gss.isSerialOpen() );

		THEN("Serial is opened") {
			REQUIRE( gss.serialOpen() );
			REQUIRE( gss.isSerialOpen() );
		}
	}

}

SCENARIO_METHOD(test::SerialBridge, "Send and valid response", "[gcode-service][send]") {

	GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
	GenericMachineSpy  gms(TEST_PORT, UART_BAUDRATE);
	REQUIRE_NOTHROW( gss.start() );
	REQUIRE_NOTHROW( gms.start() );

	WHEN("Send is called with a valid gcode #1") {
		THEN("GCode is send and receives a valid reply") {
			auto response = gss.send("G0");
			response->wait();
			REQUIRE( response->isValid() );
		}
	}

	WHEN("Send is called with a valid gcode #2") {
		THEN("GCode is send and receives a valid reply") {
			auto response = gss.send("G0X0.4Y-1.3Z2.0F500");
			response->wait();
			REQUIRE( response->isValid() );
		}
	}

	WHEN("Send is called with a valid gcode #3") {
		THEN("GCode is send and receives a valid reply [1000x]") {
			for(unsigned i=0; i<1000; i++) {
				auto response = gss.send("G0");
				response->wait();
				REQUIRE( response->isValid() );
			}
		}
	}

	WHEN("Send is called with a valid gcode #4") {
		THEN("GCode is send and receives a valid reply [1000x]") {
			for(unsigned i=0; i<1000; i++) {
				auto response = gss.send("G0X0.4Y-1.3Z2.0F500");
				response->wait();
				REQUIRE( response->isValid() );
			}
		}
	}

}

SCENARIO_METHOD(test::SerialBridge, "Send and abort", "[gcode-service][send][abort]") {

	GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
	GenericMachineSpy  gms(TEST_PORT, UART_BAUDRATE);
	REQUIRE_NOTHROW( gss.start() );
	REQUIRE_NOTHROW( gms.start() );

	WHEN("Suspend is called when a command is running") {
		gms.addResponder("G0", [&gms](const std::string& line){
			// G0 will not send any reply
			return true;
		});

		Delayed abort(500, [&gss](){
			gss.suspend() ;
		});

		THEN("Command is not aborted but expires") {
			using namespace std::literals;

			abort.start();
			auto response = gss.send("G0");
			REQUIRE_FALSE( response->isValid() );
			REQUIRE_FALSE( response->isAborted() );
			REQUIRE( response->isExpired() );
		}
	}

	WHEN("Forced suspend is called and a sync command is running") {
		gms.addResponder("G0", [&gms](const std::string& line){
			// G0 will not send any reply
			return true;
		});

		Delayed abort(100, [&gss](){
			REQUIRE( gss.suspend(true) );
		});

		THEN("Command is aborted") {
			abort.start();
			auto response = gss.send("G0");
			REQUIRE_FALSE( response->isValid() );
			REQUIRE( response->isAborted() );
			REQUIRE_FALSE( response->isExpired() );
		}
	}

	WHEN("serialClose is called and a sync command is running") {
		gms.addResponder("G0", [&gms](const std::string& line){
			// G0 will not send any reply
			return true;
		});

		Delayed abort(100, [&gss](){
			REQUIRE_FALSE( gss.serialClose() );
		});

		THEN("Command is not aborted but expires") {
			abort.start();
			auto response = gss.send("G0");
			REQUIRE_FALSE( response->isValid() );
			REQUIRE_FALSE( response->isAborted() );
			REQUIRE( response->isExpired() );
		}
	}

	WHEN("Forced serialClose is called when a command is running") {
		gms.addResponder("G0", [&gms](const std::string& line){
			// G0 will not send any reply
			return true;
		});

		Delayed abort(100, [&gss](){
			REQUIRE( gss.serialClose(true) );
		});

		THEN("Command is aborted") {
			abort.start();
			auto response = gss.send("G0");
			REQUIRE_FALSE( response->isValid() );
			REQUIRE( response->isAborted() );
			REQUIRE_FALSE( response->isExpired() );
		}
	}

// async command is running and serialCLose is called

}

SCENARIO_METHOD(test::SerialBridge, "Send and timeout", "[gcode-service][send][timeout]") {

	GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
	GenericMachineSpy  gms(TEST_PORT, UART_BAUDRATE);

	REQUIRE_NOTHROW( gss.start() );
	REQUIRE_NOTHROW( gms.start() );

	WHEN("A command is sent and no reply is received") {
		gms.addResponder("G0", [&gms](const std::string& line){
			// G0 will not send any reply
			return true;
		});

		THEN("Command status is set to expired") {
			auto response = gss.send("G0");
			REQUIRE_FALSE( response->isValid() );
			REQUIRE( response->isExpired() );
		}

	}

}

SCENARIO_METHOD(test::SerialBridge, "Asynchronous send", "[gcode-service][send-async]") {

	GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
	GenericMachineSpy  gms(TEST_PORT, UART_BAUDRATE);
	WampSessionMock wamp;

	gss.setWampSession(&wamp);

	REQUIRE_NOTHROW( gss.start() );
	REQUIRE_NOTHROW( gms.start() );

	WHEN("sendAsync is called with a valid gcode #1") {

		THEN("gcode id is returned and a reply line is received") {

			auto fut1 = wamp.getPublishMatchFuture("gcode.events", 2);

			unsigned id = gss.sendAsync("M83");
			REQUIRE( id != 0 );

			fut1.wait();

			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == GCODE_ASYNC_STARTED );
			REQUIRE( wamp.m_published[0].args.args_dict["id"].as_int() == id );

			REQUIRE( wamp.m_published[1].args.args_list[0].as_string() == GCODE_ASYNC_FINISHED );
			REQUIRE( wamp.m_published[1].args.args_dict["id"].as_int() == id );
		}
	}

	WHEN("sendAsync is called with a valid gcode #2") {

		gms.addResponder("M777", [&gms](const std::string& line){
			gms.send("resp #1\n");
			gms.send("resp #2\n");
			gms.send("ok\n");
			return true;
		});

		THEN("gcode id is returned and multiple reply lines are received") {

			auto fut1 = wamp.getPublishMatchFuture("gcode.events", 4);

			unsigned id = gss.sendAsync("M777");
			REQUIRE( id != 0 );

			fut1.wait();

			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == GCODE_ASYNC_STARTED );
			REQUIRE( wamp.m_published[0].args.args_dict["id"].as_int() == id );

			REQUIRE( wamp.m_published[1].args.args_list[0].as_string() == GCODE_ASYNC_REPLY );
			REQUIRE( wamp.m_published[1].args.args_dict["id"].as_int() == id );

			REQUIRE( wamp.m_published[2].args.args_list[0].as_string() == GCODE_ASYNC_REPLY );
			REQUIRE( wamp.m_published[2].args.args_dict["id"].as_int() == id );

			REQUIRE( wamp.m_published[3].args.args_list[0].as_string() == GCODE_ASYNC_FINISHED );
			REQUIRE( wamp.m_published[3].args.args_dict["id"].as_int() == id );
		}
	}

}

SCENARIO_METHOD(test::SerialBridge, "Asynchronous send timeout", "[gcode-service][send-async][timeout]") {

	GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
	GenericMachineSpy  gms(TEST_PORT, UART_BAUDRATE);
	WampSessionMock wamp;
	gss.setWampSession(&wamp);

	REQUIRE_NOTHROW( gss.start() );
	REQUIRE_NOTHROW( gms.start() );

	WHEN("sendAsync is called and no reply is received") {

		gms.addResponder("M777", [&gms](const std::string& line){
			return true;
		});

		THEN("command expires and async_timeout message is published") {
			auto fut1 = wamp.getPublishMatchFuture("gcode.events", 2);

			unsigned id = gss.sendAsync("M777");
			REQUIRE( id != 0 );

			auto status = fut1.wait_for( std::chrono::seconds(2) );
			REQUIRE( status == std::future_status::ready );

			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == GCODE_ASYNC_STARTED );
			REQUIRE( wamp.m_published[0].args.args_dict["id"].as_int() == id );

			REQUIRE( wamp.m_published[1].args.args_list[0].as_string() == GCODE_ASYNC_EXPIRED );
			REQUIRE( wamp.m_published[1].args.args_dict["id"].as_int() == id );
		}

	}
}

SCENARIO_METHOD(test::SerialBridge, "Asynchronous abort", "[gcode-service][send-async][abort]") {
	GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
	GenericMachineSpy  gms(TEST_PORT, UART_BAUDRATE);
	WampSessionMock wamp;
	gss.setWampSession(&wamp);

	REQUIRE_NOTHROW( gss.start() );
	REQUIRE_NOTHROW( gms.start() );

	WHEN("serialClose is called and an async command is running") {
		gms.addResponder("G0", [&gms](const std::string& line){
			// G0 will not send any reply
			return true;
		});

		Delayed abort(200, [&gss](){
			REQUIRE_FALSE( gss.serialClose() );
		});

		THEN("Command is not aborted but expired") {
			auto fut1 = wamp.getPublishMatchFuture("gcode.events", 2);
			abort.start();

			unsigned id = gss.sendAsync("G0");
			REQUIRE( id != 0 );

			auto status = fut1.wait_for( std::chrono::seconds(2) );
			REQUIRE( status == std::future_status::ready );

			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == GCODE_ASYNC_STARTED );
			REQUIRE( wamp.m_published[0].args.args_dict["id"].as_int() == id );

			REQUIRE( wamp.m_published[1].args.args_list[0].as_string() == GCODE_ASYNC_EXPIRED );
			REQUIRE( wamp.m_published[1].args.args_dict["id"].as_int() == id );
		}
	}	

	WHEN("Forced serialClose is called and an async command is running") {
		gms.addResponder("G0", [&gms](const std::string& line){
			// G0 will not send any reply
			return true;
		});

		Delayed abort(200, [&gss](){
			REQUIRE( gss.serialClose(true) );
		});

		THEN("Command is aborted") {
			auto fut1 = wamp.getPublishMatchFuture("gcode.events", 2);
			abort.start();

			unsigned id = gss.sendAsync("G0");
			REQUIRE( id != 0 );

			auto status = fut1.wait_for( std::chrono::seconds(2) );
			REQUIRE( status == std::future_status::ready );

			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == GCODE_ASYNC_STARTED );
			REQUIRE( wamp.m_published[0].args.args_dict["id"].as_int() == id );

			REQUIRE( wamp.m_published[1].args.args_list[0].as_string() == GCODE_ASYNC_ABORTED );
			REQUIRE( wamp.m_published[1].args.args_dict["id"].as_int() == id );
		}
	}

	WHEN("abortAsync with correct id is called") {
		gms.addResponder("G0", [&gms](const std::string& line){
			// G0 will not send any reply
			return true;
		});

		THEN("Command is aborted") {
			auto fut1 = wamp.getPublishMatchFuture("gcode.events", 2);

			unsigned id = gss.sendAsync("G0");
			REQUIRE( id != 0 );

			Delayed abort(200, [&gss, &id](){
				REQUIRE( gss.abortAsync(id) );
			});
			abort.start();

			auto status = fut1.wait_for( std::chrono::seconds(2) );
			REQUIRE( status == std::future_status::ready );

			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == GCODE_ASYNC_STARTED );
			REQUIRE( wamp.m_published[0].args.args_dict["id"].as_int() == id );

			REQUIRE( wamp.m_published[1].args.args_list[0].as_string() == GCODE_ASYNC_ABORTED );
			REQUIRE( wamp.m_published[1].args.args_dict["id"].as_int() == id );
		}
	}

	WHEN("abortAsync with wrong id is called") {
		gms.addResponder("G0", [&gms](const std::string& line){
			// G0 will not send any reply
			return true;
		});

		THEN("Command is not aborted but expires") {
			auto fut1 = wamp.getPublishMatchFuture("gcode.events", 2);

			unsigned id = gss.sendAsync("G0");
			REQUIRE( id != 0 );

			Delayed abort(200, [&gss, &id](){
				REQUIRE_FALSE( gss.abortAsync(id+1) );
			});
			abort.start();

			auto status = fut1.wait_for( std::chrono::seconds(2) );
			REQUIRE( status == std::future_status::ready );

			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == GCODE_ASYNC_STARTED );
			REQUIRE( wamp.m_published[0].args.args_dict["id"].as_int() == id );

			REQUIRE( wamp.m_published[1].args.args_list[0].as_string() == GCODE_ASYNC_EXPIRED );
			REQUIRE( wamp.m_published[1].args.args_dict["id"].as_int() == id );
		}
	}

}

// TODO: unordered async reply

SCENARIO_METHOD(test::SerialBridge, "File load operation", "[gcode-service][file][load]") {

	GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
	GenericMachineSpy  gms(TEST_PORT, UART_BAUDRATE);
	WampSessionMock wamp;
	gss.setWampSession(&wamp);

	REQUIRE_NOTHROW( gss.start() );
	REQUIRE_NOTHROW( gms.start() );

	WHEN("Valid filename is provided") {
		auto info = gss.fileInfo();
		REQUIRE( info["status"] == "NONE" );

		THEN("File is loaded") {
			REQUIRE( gss.fileLoad(SLIC3R_DIR"/PLA_tr_1.0-v2.1.gcode") );
			info = gss.fileInfo();
			REQUIRE( info["status"] == "LOADED" );
		}
	}

}

SCENARIO_METHOD(test::SerialBridge, "File load error operation", "[gcode-service][file][load][error]") {

	GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
	GenericMachineSpy  gms(TEST_PORT, UART_BAUDRATE);
	WampSessionMock wamp;
	gss.setWampSession(&wamp);

	REQUIRE_NOTHROW( gss.start() );
	REQUIRE_NOTHROW( gms.start() );

	WHEN("Invalid filename is provided") {
		auto info = gss.fileInfo();
		REQUIRE( info["status"] == "NONE" );

		THEN("File is not loaded") {
			REQUIRE_FALSE( gss.fileLoad(SLIC3R_DIR"/PLA_tr_1.0-v2.1.gcode_xyz") );
			info = gss.fileInfo();
			REQUIRE( info["status"] == "NONE" );
		}
	}

	WHEN("Valid filename is provided but service is stopped") {
		REQUIRE_NOTHROW( gss.stop() );

		THEN("File is not loaded") {
			REQUIRE_FALSE( gss.fileLoad(SLIC3R_DIR"/PLA_tr_1.0-v2.1.gcode") );
		}
	}

	WHEN("Valid filename is provided but service is suspended") {

		REQUIRE_NOTHROW( gss.suspend() );

		THEN("File is not loaded") {
			REQUIRE_FALSE( gss.fileLoad(SLIC3R_DIR"/PLA_tr_1.0-v2.1.gcode") );
		}
	}

}

SCENARIO_METHOD(test::SerialBridge, "File push operation", "[gcode-service][file][push]") {

	GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
	GenericMachineSpy  gms(TEST_PORT, UART_BAUDRATE);
	WampSessionMock wamp;
	gss.setWampSession(&wamp);

	REQUIRE_NOTHROW( gss.start() );
	REQUIRE_NOTHROW( gms.start() );

	WHEN("Valid small file is provided") {
		THEN("File is loaded and pushed") {
			auto fut1 = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 1, GCODE_FILE_STARTED);
			auto fut2 = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 1, GCODE_FILE_FINISHED);

			REQUIRE( gss.fileLoadAndPush(GENERIC_DIR"/10lines.gcode") );

			fut1.wait();
			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == GCODE_FILE_STARTED );

			fut2.wait();
			unsigned len = wamp.m_published.size();
			REQUIRE( wamp.m_published[len-1].args.args_list[0].as_string() == GCODE_FILE_FINISHED );
		}
	}

	WHEN("Valid big file is provided") {
		THEN("File is loaded and pushed") {
			auto fut1 = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 1, GCODE_FILE_STARTED);
			auto fut2 = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 1, GCODE_FILE_FINISHED);

			REQUIRE( gss.fileLoadAndPush(SLIC3R_DIR"/PLA_tr_1.0-v2.1.gcode") );

			fut1.wait();
			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == GCODE_FILE_STARTED );

			fut2.wait();
			unsigned len = wamp.m_published.size();
			REQUIRE( wamp.m_published[len-1].args.args_list[0].as_string() == GCODE_FILE_FINISHED );
		}
	}

	WHEN("Valid big file is provided") {

		// TODO: last command looses OK response, why?

		THEN("Progress values is published squentially") {
			using namespace std::literals;

			auto fut1 = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 1, GCODE_FILE_STARTED);
			auto fut2 = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 1, GCODE_FILE_FINISHED);

			REQUIRE( gss.fileLoadAndPush(SLIC3R_DIR"/PLA_tr_1.0-v2.1.gcode") );
			// REQUIRE_FALSE( gss.fileLoadAndPush(GENERIC_DIR"/10lines.gcode") );

			fut1.wait();
			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == GCODE_FILE_STARTED );

			fut2.wait();
			unsigned len = wamp.m_published.size();
			REQUIRE( wamp.m_published[len-1].args.args_list[0].as_string() == GCODE_FILE_FINISHED );

			for(unsigned i=1; i<len-1; i++) {
				auto topic = wamp.m_published[i].args.args_list[0].as_string();
				REQUIRE(  ((topic == GCODE_FILE_PROGRESS) || (topic == GCODE_ASYNC_UNORDERED)) );
			}

		}
	}

}

SCENARIO_METHOD(test::SerialBridge, "File push error operation", "[gcode-service][file][push][error]") {

	GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
	GenericMachineSpy  gms(TEST_PORT, UART_BAUDRATE);
	WampSessionMock wamp;
	gss.setWampSession(&wamp);

	REQUIRE_NOTHROW( gss.start() );
	REQUIRE_NOTHROW( gms.start() );

	WHEN("Invalid filename is provided") {
		THEN("File is not loaded and pushed") {
			REQUIRE_FALSE( gss.fileLoadAndPush(SLIC3R_DIR"/PLA_tr_1.0-v2.1.gcode_xyz") );
		}
	}	

	WHEN("Valid filename is provided bu service is stopped") {
		REQUIRE_NOTHROW( gss.stop() );

		THEN("File is not loaded and pushed") {
			REQUIRE_FALSE( gss.fileLoadAndPush(SLIC3R_DIR"/PLA_tr_1.0-v2.1.gcode") );
		}
	}

	WHEN("Valid filename is provided bu service is suspended") {
		REQUIRE( gss.suspend() );

		THEN("File is not loaded and pushed") {
			REQUIRE_FALSE( gss.fileLoadAndPush(SLIC3R_DIR"/PLA_tr_1.0-v2.1.gcode") );
		}
	}

}

SCENARIO_METHOD(test::SerialBridge, "File pause-error operation", "[gcode-service][file][pause][error]") {

	GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
	GenericMachineSpy  gms(TEST_PORT, UART_BAUDRATE);
	WampSessionMock wamp;
	gss.setWampSession(&wamp);

	REQUIRE_NOTHROW( gss.start() );
	REQUIRE_NOTHROW( gms.start() );

	WHEN("Pause is called on a stopped service") {
		REQUIRE_NOTHROW( gss.stop() );

		THEN("Pause returns false") {
			REQUIRE_FALSE( gss.filePause() );
		}
	}

	WHEN("Pause is called on a suspended service") {
		REQUIRE( gss.suspend() );

		THEN("File is not loaded and pushed") {
			REQUIRE_FALSE( gss.filePause() );
		}
	}

	WHEN("When pause is called on a not loaded file") {
		auto info = gss.fileInfo();
		REQUIRE( info["status"] == "NONE" );

		THEN("Pause returns false") {
			REQUIRE_FALSE( gss.filePause() );
		}

	}

	WHEN("When pause is called on a loaded file") {
		REQUIRE( gss.fileLoad(SLIC3R_DIR"/PLA_tr_1.4-v2.gcode") );
		auto info = gss.fileInfo();
		REQUIRE( info["status"] == "LOADED" );

		THEN("Pause returns false") {
			REQUIRE_FALSE( gss.filePause() );
		}

	}

}

SCENARIO_METHOD(test::SerialBridge, "File pause/resume operation", "[gcode-service][file-pause]") {

	GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
	GenericMachineSpy  gms(TEST_PORT, UART_BAUDRATE);
	WampSessionMock wamp;
	gss.setWampSession(&wamp);

	REQUIRE_NOTHROW( gss.start() );
	REQUIRE_NOTHROW( gms.start() );

	WHEN("Pause is called on started file push") {
		THEN("File push is paused") {
			using namespace std::literals;

			auto eventStarted = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 1, GCODE_FILE_STARTED);
			auto eventPaused = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 1, GCODE_FILE_PAUSED);
			auto eventResumed = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 1, GCODE_FILE_RESUMED);
			auto fut4 = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 1, GCODE_FILE_FINISHED);
			auto progressEvent = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 10, GCODE_FILE_PROGRESS);

			REQUIRE( gss.fileLoadAndPush(SLIC3R_DIR"/PLA_tr_1.4-v2.gcode") );

			eventStarted.wait();
			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == GCODE_FILE_STARTED );

			progressEvent.wait();

			REQUIRE( gss.filePause() );
			auto info = gss.fileInfo();
			REQUIRE( info["status"] == "PAUSED" );

			eventPaused.wait();
			unsigned len = wamp.m_published.size();
			REQUIRE( wamp.m_published[len-1].args.args_list[0].as_string() == GCODE_FILE_PAUSED );

			REQUIRE( gss.fileResume() );

			eventResumed.wait();
			info = gss.fileInfo();
			REQUIRE( info["status"] == "PUSHING" );

			fut4.wait();
			len = wamp.m_published.size();
			REQUIRE( wamp.m_published[len-1].args.args_list[0].as_string() == GCODE_FILE_FINISHED );
		}
	}	

}

SCENARIO_METHOD(test::SerialBridge, "File pause-wait/resume operation", "[gcode-service][file-pause-wait]") {

	GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
	GenericMachineSpy  gms(TEST_PORT, UART_BAUDRATE);
	WampSessionMock wamp;
	gss.setWampSession(&wamp);

	REQUIRE_NOTHROW( gss.start() );
	REQUIRE_NOTHROW( gms.start() );

	WHEN("When pause is called during a long wait command") {

		gms.addResponder("M190", [&gms](const std::string& line){
			// G0 will not send any reply
			return true;
		});

		THEN("File state is switched to PAUSED_WAIT") {

			auto fut1 = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 1, GCODE_FILE_STARTED);
			auto fut2 = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 1, GCODE_FILE_PAUSED);
			auto fut3 = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 1, GCODE_FILE_RESUMED);
			auto fut4 = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 1, GCODE_FILE_FINISHED);

			REQUIRE( gss.fileLoadAndPush(SLIC3R_DIR"/PLA_tr_1.4-v2.gcode") );

			fut1.wait();
			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == GCODE_FILE_STARTED );

			std::this_thread::sleep_for(500ms);

			REQUIRE( gss.filePause() );
			auto info = gss.fileInfo();
			REQUIRE( info["status"] == "PAUSED_WAIT" );

			std::this_thread::sleep_for(500ms);

			Delayed finish_m190(500, [&gms](){
				gms.send("ok");
			});
			finish_m190.start();			

			fut2.wait();
			unsigned len = wamp.m_published.size();
			REQUIRE( wamp.m_published[len-1].args.args_list[0].as_string() == GCODE_FILE_PAUSED );

			info = gss.fileInfo();
			REQUIRE( info["status"] == "PAUSED" );

			REQUIRE( gss.fileResume() );

			fut3.wait();
			len = wamp.m_published.size();
			REQUIRE( wamp.m_published[len-1].args.args_list[0].as_string() == GCODE_FILE_RESUMED );

			fut4.wait();
			len = wamp.m_published.size();
			REQUIRE( wamp.m_published[len-1].args.args_list[0].as_string() == GCODE_FILE_FINISHED );
		}
	}

}

SCENARIO_METHOD(test::SerialBridge, "File abort-error operation", "[gcode-service][file][abort-error]") {

	GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
	GenericMachineSpy  gms(TEST_PORT, UART_BAUDRATE);
	WampSessionMock wamp;
	gss.setWampSession(&wamp);

	REQUIRE_NOTHROW( gss.start() );
	REQUIRE_NOTHROW( gms.start() );

	WHEN("Abort is called on a stopped service") {
		REQUIRE_NOTHROW( gss.stop() );

		THEN("Abort returns false") {
			REQUIRE_FALSE( gss.fileAbort() );
		}
	}

	WHEN("Abort is called on a suspended service") {
		REQUIRE( gss.suspend() );

		THEN("File is not loaded and pushed") {
			REQUIRE_FALSE( gss.fileAbort() );
		}
	}

	WHEN("Abort is called on a not loaded file") {
		auto info = gss.fileInfo();
		REQUIRE( info["status"] == "NONE" );

		THEN("Abort returns false") {
			REQUIRE_FALSE( gss.fileAbort() );
		}

	}

	WHEN("Abort is called on a not started file") {
		REQUIRE( gss.fileLoad(SLIC3R_DIR"/PLA_tr_1.4-v2.gcode") );
		auto info = gss.fileInfo();
		REQUIRE( info["status"] == "LOADED" );

		THEN("Abort returns false") {
			REQUIRE_FALSE( gss.fileAbort() );
		}

	}

}

SCENARIO_METHOD(test::SerialBridge, "File abort operation", "[gcode-service][file][abort]") {

	GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
	GenericMachineSpy  gms(TEST_PORT, UART_BAUDRATE);
	WampSessionMock wamp;
	gss.setWampSession(&wamp);

	REQUIRE_NOTHROW( gss.start() );
	REQUIRE_NOTHROW( gms.start() );

	WHEN("Abort is called during active file push") {
		auto fut1 = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 1, GCODE_FILE_STARTED);
		auto fut2 = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 10, GCODE_FILE_PROGRESS);
		auto fut3 = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 1, GCODE_FILE_ABORTED);

		REQUIRE( gss.fileLoadAndPush(SLIC3R_DIR"/PLA_tr_1.0-v2.1.gcode") );

		THEN("File push is aborted") {
			fut1.wait();
			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == GCODE_FILE_STARTED );
			fut2.wait();

			auto info = gss.fileInfo();
			REQUIRE( info["status"] == "PUSHING" );

			REQUIRE( gss.fileAbort() );
			fut3.wait();
			info = gss.fileInfo();
			REQUIRE( info["status"] == "NONE" );
		}
	}

	WHEN("Abort is called during paused file push") {
		auto fut1 = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 1, GCODE_FILE_STARTED);
		auto fut2 = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 10, GCODE_FILE_PROGRESS);
		auto fut3 = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 1, GCODE_FILE_PAUSED);
		auto fut4 = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 1, GCODE_FILE_ABORTED);

		REQUIRE( gss.fileLoadAndPush(SLIC3R_DIR"/PLA_tr_1.0-v2.1.gcode") );

		THEN("File push is aborted") {
			fut1.wait();
			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == GCODE_FILE_STARTED );

			fut2.wait();
			auto info = gss.fileInfo();
			REQUIRE( info["status"] == "PUSHING" );

			REQUIRE( gss.filePause() );

			fut3.wait();
			info = gss.fileInfo();
			REQUIRE( info["status"] == "PAUSED" );

			REQUIRE( gss.fileAbort() );

			fut4.wait();
			info = gss.fileInfo();
			REQUIRE( info["status"] == "NONE" );
		}
	}

	WHEN("Abort is called during paused-wait file push") {
		auto fut1 = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 1, GCODE_FILE_STARTED);
		auto fut2 = wamp.getCallMatchFuture("controller.reset", 1);
		auto fut4 = wamp.getPublishMatchFuture(TOPIC_GCODE_FILE, 1, GCODE_FILE_ABORTED);

		gms.addResponder("M190", [&gms](const std::string& line){
			// M190 will not send any reply
			return true;
		});

		wamp.addCallResponder("controller.reset", [&gss](wampcc::wamp_args args) -> wampcc::result_info {

			gss.serialClose(true);
			std::this_thread::sleep_for(1s);
			gss.serialOpen();

			return {};
		});

		REQUIRE( gss.fileLoadAndPush(SLIC3R_DIR"/PLA_tr_1.0-v2.1.gcode") );

		THEN("File push is aborted and controller is reset") {
			fut1.wait();
			REQUIRE( wamp.m_published[0].args.args_list[0].as_string() == GCODE_FILE_STARTED );

			auto info = gss.fileInfo();
			REQUIRE( info["status"] == "WAIT" );

			REQUIRE( gss.filePause() );

			info = gss.fileInfo();
			REQUIRE( info["status"] == "PAUSED_WAIT" );

			REQUIRE( gss.fileAbort() );
			// Wait for call
			fut2.wait();
			// Wait for aborted
			fut4.wait();
			info = gss.fileInfo();
			REQUIRE( info["status"] == "NONE" );
		}
	}

}

// bool fileSetNotifyParams(float min_progress, bool status_update, bool height_update);

SCENARIO_METHOD(test::SerialBridge, "File info operation", "[gcode-service][file-info]") {

	GCodeServiceSpy gss(UART_PORT, UART_BAUDRATE);
	GenericMachineSpy  gms(TEST_PORT, UART_BAUDRATE);
	WampSessionMock wamp;
	gss.setWampSession(&wamp);

	REQUIRE_NOTHROW( gss.start() );
	REQUIRE_NOTHROW( gms.start() );

	THEN("") {

		auto info = gss.info();

		std::cout << info << std::endl;

	}
	
}

// bool fileSetOverride(const std::string& override, float value, unsigned index);

// wampcc::json_object fileInfo();