#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>

#include "machine/generic.hpp"
#include "serial_bridge.hpp"
#include <vector>
#include <serialxx/thread_safe.hpp>

/****** Constants *******/
constexpr char UART_PORT[] = "/tmp/ttyDUT0";
constexpr char TEST_PORT[] = "/tmp/ttyDUT1";
constexpr unsigned UART_BAUDRATE = 115000;

/******* Helpers *******/
auto bridge = test::SerialBridge::getInstance();

class GenericMachineSpy: public test::GenericMachine {
	public:
		GenericMachineSpy(const std::string& portName, unsigned baudRate=115200, unsigned timeout=1000) 
			: test::GenericMachine(portName, baudRate, timeout) { }

		bool isSerialOpen() {
			return m_serial.isOpen();
		}

		std::vector<std::string> reply;

		bool onReceive(const std::string& line) {
			reply.push_back(line);
			return false;
		}
};

using namespace test;
using namespace std::literals;

SCENARIO_METHOD(test::SerialBridge, "New object creation", "[machine][generic]") {

	WHEN("New object is created") {
		GenericMachineSpy *m;

		THEN("No exception is thrown") {
			REQUIRE_NOTHROW( m = new GenericMachineSpy(UART_PORT, UART_BAUDRATE) );
			REQUIRE_NOTHROW( delete m );
		}
	}

}

SCENARIO_METHOD(test::SerialBridge, "Start and stop", "[machine][generic]") {

	WHEN("Start is called on a new object") {
		GenericMachineSpy m(UART_PORT, UART_BAUDRATE);

		THEN("No exception is thrown and serial is open") {
			REQUIRE_NOTHROW( m.start() );
			REQUIRE( m.isSerialOpen() );
		}
	}

	WHEN("Stop is called on a new object") {
		GenericMachineSpy m(UART_PORT, UART_BAUDRATE);

		THEN("No exception is thrown") {
			REQUIRE_NOTHROW( m.stop() );
		}
	}

	WHEN("Stop is called on a started object") {
		GenericMachineSpy m(UART_PORT, UART_BAUDRATE);
		REQUIRE_NOTHROW( m.start() );
		REQUIRE( m.isSerialOpen() );

		THEN("No exception is thrown") {
			REQUIRE_NOTHROW( m.stop() );
			REQUIRE_FALSE( m.isSerialOpen() );
		}
	}

}

SCENARIO_METHOD(test::SerialBridge, "Send and receive", "[machine][generic]") {

	WHEN("Data is send over serial") {
		GenericMachineSpy m(UART_PORT, UART_BAUDRATE);
		REQUIRE_NOTHROW( m.start() );
		REQUIRE( m.isSerialOpen() );

		THEN("Data is received") {
			serialxx::thread_safe::Serial s(TEST_PORT, UART_BAUDRATE, 100);
			s.open();

			REQUIRE( s.isOpen() );

			for(unsigned i=0; i<100; i++) {
				std::string line;
				s.write("G0\n");
				s.readLine(line);

				REQUIRE( line == "ok\n" );
			}
		}
	}

}