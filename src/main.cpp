
/*
 * Copyright (c) 2017 Darren Smith
 *
 * wampcc is free software; you can redistribute it and/or modify
 * it under the terms of the MIT license. See LICENSE for details.
 */

#include <memory>
#include <iostream>
#include <CLI/CLI.hpp>

#include "config.h"
#include "gcode_service_wamp_wrapper.hpp"

void show_version(size_t) {
	std::cout << PACKAGE_STRING << std::endl; 
	throw CLI::Success();
}

int main(int argc, char** argv)
{
	CLI::App app{PACKAGE_NAME};

	std::string uri   	 = "ws://127.0.0.1:9001";
	std::string realm 	 = "fabui";
	std::string	env_file = "/mnt/projects/Colibri-Embedded/fabui/fabui-frontend/application/.env";
	std::string serial_port = "/dev/ttyACM0";
	unsigned baudrate    = 115000;

	app.add_option("-u,--uri", uri, "WAMP default realm [default: " + uri + "]");
	app.add_option("-r,--realm", realm, "WAMP default realm [default: " + realm + "]");
	app.add_option("-e,--backend-env", env_file, "FABUI web .env file [default: " + env_file + "]");
	app.add_option("-p,--serial-port", serial_port, "Serial port [default: " + serial_port + "]");
	app.add_option("-b,--baudrate", serial_port, "Serial baudrate [default: " + std::to_string(baudrate) + "]");
	app.add_config("-c,--config", "config.ini", "Read config from an ini file", false);
	app.add_flag_function("-v,--version", show_version, "Show version");

	try {
		app.parse(argc, argv);
	} catch (const CLI::ParseError &e) {
		return app.exit(e);
	}	
	
	try {
		
		fabui::GCodeService gcodeservice(serial_port, baudrate);
		fabui::GCodeServiceWrapper wampservice(env_file, gcodeservice);
		//gs.run("127.0.0.1", port, default_realm);
		if( wampservice.connect(uri, realm)) {
			auto finished = wampservice.finished_future();
			finished.wait();
		}
		
	} catch (const std::exception& e) {
		std::cout << e.what() << std::endl;
		return 1;
	}
}
