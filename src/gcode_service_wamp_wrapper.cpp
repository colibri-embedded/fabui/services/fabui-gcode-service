/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: gcode_service_wamp_wrapper.cpp
 * @brief GCodeService wamp wrapper implementation
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#include "config.h"
#include "gcode_service_wamp_wrapper.hpp"

#define MAX_WORKERS		8

using namespace fabui;
using namespace wampcc;
using namespace std::placeholders;

GCodeServiceWrapper::GCodeServiceWrapper(const std::string& env_file, GCodeService& service)
	: BackendService(PACKAGE_STRING, env_file, MAX_WORKERS)
	, m_service(service)
{
	// m_service.publish_fn = std::bind(&GCodeServiceWrapper::publish, this, _1, _2, _3);
	// m_service.call_fn = std::bind(&GCodeServiceWrapper::call, this, _1, _2, _3);
	m_service.setWampSession(this);
}

void GCodeServiceWrapper::onConnect()
{
	join( realm(), {"token"}, "gcodeservice");
}

void GCodeServiceWrapper::onDisconnect()
{
	//m_service.terminate();
	m_service.stop();
}

void GCodeServiceWrapper::onJoin()
{
	std::cout << "Joined\n";

	// try to get machine.info
	auto cfut = call("machine.info");
	cfut.wait();
	auto result = cfut.get();

	// std::cout << "MACHINE: " << result.args.args_dict << std::endl;

	provide("gcode.service.terminate", std::bind(&GCodeServiceWrapper::terminate_wrapper, this, _1));
	provide("gcode.service.suspend", std::bind(&GCodeServiceWrapper::suspend_wrapper, this, _1));
	provide("gcode.service.resume", std::bind(&GCodeServiceWrapper::resume_wrapper, this, _1));

	provide("serial.close", std::bind(&GCodeServiceWrapper::serialClose_wrapper, this, _1));	
	provide("serial.open", std::bind(&GCodeServiceWrapper::serialOpen_wrapper, this, _1));	

	provide("gcode.send", 	std::bind(&GCodeServiceWrapper::send_wrapper, this, _1));
	provide("gcode.async.send", 	std::bind(&GCodeServiceWrapper::sendAsync_wrapper, this, _1));
	provide("gcode.async.abort", 	std::bind(&GCodeServiceWrapper::abortAsync_wrapper, this, _1));

	provide("gcode.file.load",		std::bind(&GCodeServiceWrapper::fileLoad_wrapper, this, _1));
	provide("gcode.file.push",		std::bind(&GCodeServiceWrapper::filePush_wrapper, this, _1));
	provide("gcode.file.load_and_push",		std::bind(&GCodeServiceWrapper::fileLoadAndPush_wrapper, this, _1));

	provide("gcode.file.abort",		std::bind(&GCodeServiceWrapper::fileAbort_wrapper, this, _1));
	provide("gcode.file.pause",		std::bind(&GCodeServiceWrapper::filePause_wrapper, this, _1));
	provide("gcode.file.resume",	std::bind(&GCodeServiceWrapper::fileResume_wrapper, this, _1));

	provide("gcode.file.info",		std::bind(&GCodeServiceWrapper::fileInfo_wrapper, this, _1));
	provide("gcode.file.set_notify_params",		std::bind(&GCodeServiceWrapper::fileSetNotifyParams_wrapper, this, _1));

	provide("gcode.file.set_override", std::bind(&GCodeServiceWrapper::fileSetOverride_wrapper, this, _1));

	if( m_service.start() ) {
		std::cout << "GCodeService started\n" << std::flush;
	}
}

void GCodeServiceWrapper::terminate_wrapper(invocation_info info) {
	yield(info.request_id);
	disconnect();
}

void GCodeServiceWrapper::suspend_wrapper(invocation_info info) {
	std::cout << "suspend_wrapper\n";
	try {

		bool force = getArgumentOrDefault("force", info.args.args_dict, false).as_bool();
		auto result = m_service.suspend(force);
		yield(info.request_id, {result} );

	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

void GCodeServiceWrapper::resume_wrapper(invocation_info info) {
	try {
		m_service.resume();
		yield(info.request_id);
	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

void GCodeServiceWrapper::info_wrapper(invocation_info info) {
	try {
		auto result = m_service.info();
		yield(info.request_id, {}, result);
	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

void GCodeServiceWrapper::serialOpen_wrapper(invocation_info info) {
	try {

		auto result = m_service.serialOpen();
		yield(info.request_id, {result} );

	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

void GCodeServiceWrapper::serialClose_wrapper(invocation_info info) {
	try {
		bool force = false;

		if(info.args.args_list.size() == 1) {
			force = getArgumentOrFail(0, info.args.args_list).as_bool();
		} else {
			force = getArgumentOrDefault("force", info.args.args_dict, false).as_bool();
		}
		

		auto result = m_service.serialClose(force);
		yield(info.request_id, {result} );

	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

void GCodeServiceWrapper::serialInfo_wrapper(wampcc::invocation_info info) {
	try {
		auto result = m_service.serialInfo();
		yield(info.request_id, {}, result );

	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}	
}

void GCodeServiceWrapper::send_wrapper(wampcc::invocation_info info) {
	try {

		std::string gcode = getArgumentOrFail("gcode", info.args.args_dict).as_string();
		std::string reply = getArgumentOrDefault("reply", info.args.args_dict, "ok.*").as_string();
		bool use_regex 	  = getArgumentOrDefault("use_regex", info.args.args_dict, true).as_bool();
		unsigned timeout  = getArgumentOrDefault("timeout", info.args.args_dict, 1000).as_int();

		std::cout << "send_wrapper: " << gcode << std::endl;

		auto result = m_service.send(gcode, reply, use_regex, timeout);
		std::cout << "send_wrapper(finished): " << gcode << std::endl;

		yield(info.request_id, {}, result->toObject() );

	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

void GCodeServiceWrapper::sendAsync_wrapper(invocation_info info) {
	std::cout << "sendAsync_wrapper\n";
	try {
		std::string gcode = getArgumentOrFail("gcode", info.args.args_dict).as_string();
		std::string reply = getArgumentOrDefault("reply", info.args.args_dict, "ok").as_string();
		bool use_regex 	  = getArgumentOrDefault("use_regex", info.args.args_dict, false).as_bool();
		unsigned timeout  = getArgumentOrDefault("timeout", info.args.args_dict, 1000).as_int();

		auto result = m_service.sendAsync(gcode, reply, use_regex, timeout);
		yield(info.request_id, {result} );

	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

void GCodeServiceWrapper::abortAsync_wrapper(invocation_info info) {
	try {
		unsigned id = getArgumentOrFail(0, info.args.args_list).as_int();

		auto result = m_service.abortAsync(id);
		yield(info.request_id, {result} );

	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

void GCodeServiceWrapper::fileLoad_wrapper(invocation_info info) {
	try {
		std::string filename = getArgumentOrDefault("filename", info.args.args_dict, "").as_string();
		if(filename.empty())
			filename = getArgumentOrFail(0, info.args.args_list).as_string();

		auto result = m_service.fileLoad(filename);
		yield(info.request_id, {result} );

	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

void GCodeServiceWrapper::filePush_wrapper(invocation_info info) {
	try {

		auto result = m_service.filePush();
		yield(info.request_id, {result} );

	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

void GCodeServiceWrapper::fileLoadAndPush_wrapper(invocation_info info) {
	try {
		std::string filename = getArgumentOrDefault("filename", info.args.args_dict, "").as_string();
		if(filename.empty())
			filename = getArgumentOrFail(0, info.args.args_list).as_string();

		auto result = m_service.fileLoadAndPush(filename);
		yield(info.request_id, {result} );

	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

void GCodeServiceWrapper::filePause_wrapper(wampcc::invocation_info info) {
	try {
		auto result = m_service.filePause();
		yield(info.request_id, {result} );

	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

void GCodeServiceWrapper::fileResume_wrapper(wampcc::invocation_info info) {
	try {
		auto result = m_service.fileResume();
		yield(info.request_id, {result} );

	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

void GCodeServiceWrapper::fileAbort_wrapper(wampcc::invocation_info info) {
	try {
		auto result = m_service.fileAbort();
		yield(info.request_id, {result} );

	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

void GCodeServiceWrapper::fileInfo_wrapper(invocation_info info) {
	try {

		auto result = m_service.fileInfo();
		yield(info.request_id, {}, result);

	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

void GCodeServiceWrapper::fileSetOverride_wrapper(wampcc::invocation_info info)
{
	try {
		std::string type = getArgumentOrFail("type", info.args.args_dict).as_string();
		float value 	 = getArgumentOrFail("value", info.args.args_dict).as_real();
		unsigned index   = getArgumentOrDefault("index", info.args.args_dict, 0).as_int();

		auto result = m_service.fileSetOverride(type, value, index);
		yield(info.request_id, {result} );

	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}

void GCodeServiceWrapper::fileSetNotifyParams_wrapper(wampcc::invocation_info info) {
	try {
		float min_progress = getArgumentOrDefault("min_progress", info.args.args_dict, 0.1f).as_real();
		bool status_update = getArgumentOrDefault("status_update", info.args.args_dict, false).as_bool();
		bool height_update = getArgumentOrDefault("height_update", info.args.args_dict, false).as_bool();

		auto result = m_service.fileSetNotifyParams(min_progress, status_update, height_update);
		yield(info.request_id, {result});
	} catch(const std::exception &e) {
		/* Return error */
		json_array errors = { e.what() };
		error(info.request_id, "wamp.error.runtime_error", errors);
	}
}