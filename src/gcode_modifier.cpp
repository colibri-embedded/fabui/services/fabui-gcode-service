/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: gcode_modifier.cpp
 * @brief GCode modifier implementation
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#include "gcode_modifier.hpp"
#include "gcode_hashed.hpp"
#include "gcode.hpp"
#include "gcode_types.hpp"
#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <iostream>

#define NO_INDEX unsigned(-1)

using namespace fabui;

GCodeModifier::GCodeModifier(GCodeState &state) 
	: m_state(state)
{
	GCodeOverride* modifier;

	// Nozzle_temp-Override
	auto nozzle_temp_transform = [this](GCodeOverride *self, GCode& gc, std::string&, bool& skip) {
		if( (gc == M104 or gc == M109) and self->locked) {
			skip = true;
			return true;
		}
		return true;
	};
	auto nozzle_temp_generate = [this](GCodeOverride *self, float, float new_value) {
		m_lines.push( fmt::format("M104 S{0} T{1}", new_value, self->index) );
	};

	for(unsigned i=0; i<MAX_NOZZLE_COUNT; i++) {
		modifier = addModifier("nozzle_temp", i);
		modifier->transform = nozzle_temp_transform;
		modifier->generate = nozzle_temp_generate;
	}

	// Bed_temp-Override
	modifier = addModifier("bed_temp");
	modifier->transform = [this](GCodeOverride *self, GCode& gc, std::string&, bool& skip){
		if( (gc == M140 or gc == M190) and self->locked ) {
			skip = true;
			return false;
		}
		return true;
	};
	modifier->generate = [this](GCodeOverride*, float, float new_value) {
		m_lines.push( fmt::format("M140 S{0}", new_value) );
	};
	//  FAN-Override
	modifier = addModifier("fan", 0, 100.0f);
	modifier->transform = [this](GCodeOverride *self, GCode& gc, std::string&, bool& skip){
		if( (gc == M106 or gc == M107) and self->locked ) {
			skip = true;
			return false;
		}
		return true;
	};
	modifier->generate = [this](GCodeOverride*, float, float new_value) {
		if(new_value) {
			unsigned pwm = unsigned( (255 * new_value) / 100.0f);
			m_lines.push( fmt::format("M106 S{0}", pwm) );
		}
		else
			m_lines.push("M107");
	};

	// PWM-Override
	//modifier = addModifier("pwm");
	// RPM-Override
	/*modifier = addModifier("rpm");
	modifier->transform = [this](GCodeOverride *self, GCode& gc, std::string& gcode){
		if(gc == M220 and self->locked) {
			return true;
		}
		return false;
	};
	modifier->generate = [this](GCodeOverride*, float old_value, float new_value) {
		m_lines.push( fmt::format("M220 S{0}", new_value) );
	};*/

	// Flow-override
	modifier = addModifier("flow", 0, 100.0f);
	modifier->transform = [this](GCodeOverride *self, GCode& gc, std::string&, bool& skip){
		if( gc == M221 and self->locked ) {
			skip = true;
			return false;
		}
		return true;
	};
	modifier->generate = [this](GCodeOverride*, float, float new_value) {
		m_lines.push( fmt::format("M221 S{0}", new_value) );
	};

	// Speed-Override
	modifier = addModifier("speed", 0, 100.0f);
	modifier->transform = [this](GCodeOverride *self, GCode& gc, std::string&, bool& skip){
		if( gc == M220 and self->locked ) {
			skip = true;
			return false;
		}
		return true;
	};
	modifier->generate = [this](GCodeOverride*, float, float new_value) {
		m_lines.push( fmt::format("M220 S{0}", new_value) );
	};

	// Z-Override
	modifier = addModifier("z");
	modifier->transform = [this](GCodeOverride *self, GCode& gc, std::string& gcode, bool&){
		if(gc != G0 and gc != G1) {
			return true;
		}

		try {
			float z = gc.getFloat('Z');
			z += self->value;
			gc.setFloat('Z', z);
			gcode = gc.to_string();
		} catch(...) {
			return false;
		}
		return false;
	};

	modifier->generate = [this](GCodeOverride*, float old_value, float new_value) {
		float z_diff = new_value - old_value;
		// Generate new lines
		m_lines.push("G91");
		m_lines.push( fmt::format("G0Z{0}", z_diff ) );
		//m_lines.push("M400");
		m_lines.push("G90");
	};
}

GCodeOverride* GCodeModifier::addModifier(const std::string& name, unsigned index, float value) 
{
	auto raw = new GCodeOverride{
		name, 
		index, 
		value,
		false,
		false,
		nullptr,
		nullptr,
		nullptr
	};
	auto modifier = std::unique_ptr<GCodeOverride>(raw);
	if(value != 0.0f) {
		modifier->reset = [value](GCodeOverride *self) {
			self->value = value;
		};
	}
	modifiers.push_front( std::move(modifier) );
	return raw;
}

bool GCodeModifier::nextLine(std::string& gcode, std::string& comment) 
{
	comment = "";
	return m_lines.try_pop(gcode);
}

void GCodeModifier::pushLine(const std::string& line)
{
	m_lines.push(line);
}

bool GCodeModifier::setOverride(const std::string& name, float value, unsigned index)
{
	for(auto &modifier : modifiers) {
		
		if(modifier->name == name and modifier->index == index) {
			bool changed = (value != modifier->value) or (not modifier->active);

			if(modifier->generate and changed) {
				modifier->generate(modifier.get(), modifier->value, value);
			}

			modifier->value = value;
			modifier->active = true;
			return changed;
		}
	}

	throw std::runtime_error("override type is not supported");
}

void GCodeModifier::setLocked(const std::string& name, bool locked, unsigned index) 
{
	for(auto &modifier : modifiers) {
		if(modifier->name == name and modifier->index == index) {
			modifier->locked = locked;
			return;
		}
	}

	throw std::runtime_error("override type is not supported");
}

std::tuple<bool, bool, float> GCodeModifier::getOverride(const std::string& name, unsigned index) 
{
	for(auto &modifier : modifiers) {
		//std::cout << modifier->name << std::endl;
		if(modifier->name == name and modifier->index == index) {
			return std::make_tuple(modifier->active, modifier->locked, modifier->value);
		}
	}

	throw std::runtime_error("override type is not supported");
}

wampcc::json_object GCodeModifier::info()
{
	wampcc::json_object override;

	for(auto &modifier : modifiers) {
		override[ modifier->name ] = wampcc::json_object{
			{"active", modifier->active},
			{"locked", modifier->locked},
			{"value", modifier->value}
		};
	}

	return override;
}

bool GCodeModifier::transform(GCode& gcode_obj, std::string& gcode, const std::string& /*comment*/) 
{
	bool skip = false;

	for(auto &modifier : modifiers) {
		if(modifier->transform and modifier->active) {
			if(not modifier->transform(modifier.get(), gcode_obj, gcode, skip) ) {
				return skip;
			}
		}
		
	}

	return skip;
}

void GCodeModifier::reset(const std::string& name, unsigned index)
{
	bool all_names = (name == "all");
	bool all_indexes = (index == NO_INDEX) or all_names;
	
	for(auto &modifier : modifiers) {
		if( (modifier->name == name or all_names) and (modifier->index == index or all_indexes) ) {
				modifier->active = false;
				modifier->locked = false;

				if(modifier->reset) {
					modifier->reset(modifier.get());
				} else {
					modifier->value = 0.0f;
				}
		}
	}
}

void GCodeModifier::restore(const std::string& name, unsigned index)
{
	bool all_names = (name == "all");
	bool all_indexes = (index == NO_INDEX) or all_names;

	for(auto &modifier : modifiers) {
		if( (modifier->name == name or all_names) and (modifier->index == index or all_indexes) ) {
			if(not modifier->locked) {
				modifier->active = false;

				if(modifier->reset) {
					modifier->reset(modifier.get());
				} else {
					modifier->value = 0.0f;
				}
			}
		}
	}
}

/*void GCodeModifier::process(const GCode& gcode_obj, const std::string& comment)
{
	switch(gcode_obj.hash) {
		case G0:	// extract z height
		case G1:	// extract feedrate,
			break;
		case G90:	// xyz absolute mode
			break;
		case G91:	// xyz relative mode
			break;
		case M82:	// e absolute mode
			break;
		case M83:	// e relative mode
			break;
		case M3:	// spindle CCW / laser ON
		case M4:	// spindle CW / laser ON
		case M5:	// spindle STOP / laser OFF
			break;
		case M117:	// display message
			break;
		case M104:	// set nozzle temp
		case M109:	// set bed temp
		case M140:	// set nozzle temp and wait
		case M190:	// set bed temp and wait
			break;
		case M106:	// set fan PWM
			break;
		case M107:	// fan OFF
			break;
		case M220:	// set speed factor
			break;
		case M221:	// set flow factor
			break;
		// case M240:	// trigger camera
		// 	break;
		case M401:	// deploy probe
			break;
		case M402: // stow probe
			break;

	}
}*/