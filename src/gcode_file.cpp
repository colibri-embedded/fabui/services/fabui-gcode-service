/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: gcode_file.cpp
 * @brief GCode file implementation
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#include "gcode_file.hpp"
#include <iostream>
#include <fabui/utils/string.hpp>

using namespace fabui;

GCodeFile::GCodeFile(const std::string& fileName)
	: m_filename(fileName)
	, m_total_lines(0)
	, m_current_line(0)
{
	if(not m_filename.empty())
		open();
}

GCodeFile::GCodeFile()
	: m_total_lines(0)
	, m_current_line(0)
{

}

GCodeFile::~GCodeFile()
{
	this->close();
}

bool GCodeFile::isOpened()
{
	std::lock_guard<std::mutex> guard(m_shared_mut);
	return file.is_open();
}

bool GCodeFile::open(const std::string& fileName)
{
	std::lock_guard<std::mutex> guard(m_shared_mut);
	if(file.is_open())
		return false;

	if(not fileName.empty()) {
		m_filename = fileName;
	}

	if(m_filename.empty())
		return false;

	try {
		file.open(m_filename, std::ifstream::in);
		return processFile();
	} catch(...) {
		return false;
	}
	return true;
}

bool GCodeFile::processFile()
{
	// std::cout << "processing... " << m_filename << "\n";
	if(not file.is_open())
		return false;

	unsigned lines = 0;
	std::string gcode, comment;
	while(not file.eof()) {
		nextLine(gcode, comment, true);
		lines++;
	}
	file.clear();
	file.seekg(0, file.beg);
	m_total_lines = lines;
	m_current_line = 0;
	// std::cout << "num-of-lines: " << lines << std::endl << std::flush;
	return true;
}

void GCodeFile::close()
{
	std::lock_guard<std::mutex> guard(m_shared_mut);
	if(not file.is_open())
		return;

	try {
		file.close();
	} catch(...) {

	}

	reset();
}

void GCodeFile::reset()
{
	m_total_lines = 0;
	m_current_line = 0;
}

bool GCodeFile::eof()
{
	std::lock_guard<std::mutex> guard(m_shared_mut);
	return file.eof();
}

std::string GCodeFile::getFileName()
{
	return m_filename;
}

bool GCodeFile::nextLine(std::string& gcode, std::string& comment, bool no_guard)
{
	std::string line;
	if(no_guard) {
		if(file.eof())
			return false;

		getline(file, line);
	} else {
		std::lock_guard<std::mutex> guard(m_shared_mut);
		if(file.eof())
			return false;
			
		getline(file, line);
		m_current_line++;
	}
	
	gcode.clear();
	comment.clear();

	auto pos = line.find_first_of(';');
	if(pos == std::string::npos) {
		// gcode = utils::str::rtrim(line);
		gcode = line;
		utils::str::rtrim_ref(gcode);
		return true;
	} else {
		gcode = line.substr(0, pos);
		utils::str::rtrim_ref(gcode);
		
		if(pos < line.length()-1) {
			comment = line.substr(pos+1);
			utils::str::ltrim_ref(comment);
		}
		return true;
	}

	return false;
}

unsigned GCodeFile::getNumberOfLines() {
	std::lock_guard<std::mutex> guard(m_shared_mut);
	return m_total_lines;
}

unsigned GCodeFile::getCurrentLineNumber() {
	std::lock_guard<std::mutex> guard(m_shared_mut);
	return m_current_line;
}

float GCodeFile::getProgress() {
	std::lock_guard<std::mutex> guard(m_shared_mut);
	if(m_total_lines == 0)
		return 0.0f;
	
	return ((float)m_current_line / (float)m_total_lines) * 100.0f;
}