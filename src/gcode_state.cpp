#include "gcode_state.hpp"

using namespace fabui;

GCodeState::GCodeState() 
{
	reset();
}

void GCodeState::reset() 
{
	/* Machine */
	speed = 100.0f;
	flow = 100.0f;
	
	xyz_mode = gcode::Positioning::ABSOLUTE;
	e_mode = gcode::Positioning::ABSOLUTE;

	/* Feedrates */
	feedrate = 0;

	/* Fan */
	fan_pwm = 0;

	/* Position */
	z = 0.0;

	/* Spindle */
	rpm = 0;
	rotation = gcode::Rotation::CW;

	/* Temperature */
	bed_target = 0.0f;
	nozzle_target[0] = 0.0f;
	nozzle_target[1] = 0.0f;
	nozzle_target[2] = 0.0f;
	nozzle_target[3] = 0.0f;
	nozzle_target[4] = 0.0f;
}