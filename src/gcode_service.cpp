/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: gcode_service.cpp
 * @brief GCode WAMP service implementation
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#include "config.h"
#include "gcode_service.hpp"
#include "topics.hpp"
#include <fabui/utils/string.hpp>
#include <cassert>
#include <iostream>

using namespace fabui;
using namespace wampcc;
using namespace std::placeholders;

GCodeService::GCodeService(const std::string& portName, unsigned baudRate, unsigned timeout)
	: m_timeout(timeout)
	, m_running_state(State::STOPPED)
	, m_modifier(m_state)
	, m_monitor(m_state, m_modifier)
{
	m_serial.setPortName(portName);
	m_serial.setBaudRate(baudRate);
	m_serial.setTimeout(timeout);
	m_file_status = FileStatus::NONE;
	m_async_id = ASYNC_START_ID;

	// tx_count = 0;
	// rx_count = 0;
}

GCodeService::~GCodeService()
{
	// std::cout << "gcode-service: destroy\n";
	// std::cout << "gsTX: " << tx_count << std::endl;
	// std::cout << "gsRX: " << rx_count << std::endl;
	stop();
}

void GCodeService::publish(const std::string& topic, wampcc::wamp_args args, wampcc::json_object options)
{
	// assert( publish_fn != nullptr );
	pq.push( {topic, args, options} );
}

// std::future<wampcc::result_info> GCodeService::call(const std::string& procedure, wampcc::wamp_args args, wampcc::json_object options)
// {
// 	assert( call_fn != nullptr );
// 	return call_fn(procedure, args, options);
// }

void GCodeService::setWampSession(IWampSession* session)
{
	// m_session = dynamic_cast<IWampSession*>(session);
	m_session = session;
}

IWampSession* GCodeService::getWampSession()
{
	assert( m_session != nullptr );
	return m_session;
}


plugin::gcode::Plugin* GCodeService::loadGcodePlugin(const std::string& filename)
{
	void *hndl = dlopen(filename.c_str(), RTLD_NOW);

	if(hndl == nullptr) {
		std::cerr << dlerror() << std::endl;
		return nullptr;
	}

	void *mkr = dlsym(hndl, "create");
	if(mkr == nullptr) {
		std::cerr << dlerror() << std::endl;
		return nullptr;
	}

	auto create = (plugin::gcode::create_fn_t)mkr;
	return create(nullptr);
}

void GCodeService::stop() {
	terminate();
	if(m_sender_thread.joinable())
		m_sender_thread.join();
	// std::cout << "sender: finished\n";

	if(m_receiver_thread.joinable())
		m_receiver_thread.join();
	// std::cout << "receiver: finished\n";

	if(m_timeout_thread.joinable())
		m_timeout_thread.join();
	// std::cout << "timeout: finished\n";

	if(m_publisher_thread.joinable())
		m_publisher_thread.join();
	// std::cout << "publisher: finished\n";

	m_serial.close();
}

bool GCodeService::start()
{
	try {
		if( not m_serial.open() )
			return false;

		{
			std::lock_guard<std::mutex> guard(m_running_mut);
			if(m_running_state == State::RUNNING or m_running_state == State::SUSPENDED)
				return false;
			m_running_state = State::RUNNING;
		}

		m_sender_thread = std::thread(&GCodeService::senderThreadLoop, this);
		m_receiver_thread = std::thread(&GCodeService::receiverThreadLoop, this);
		m_publisher_thread = std::thread(&GCodeService::publisherThreadLoop, this);
		m_timeout_thread = std::thread(&GCodeService::timeoutThreadLoop, this);


		e_rx_state_changed.wait();
		e_tx_state_changed.wait();

		e_rx_state_changed.clear();
		e_tx_state_changed.clear();
	} catch (...) {
		// TODO: error message for the logs
		return false;
	}

	return true;
 /*else {
		std::cout << "Error starting serial\n";
	}

	std::cout << "GCodeService started\n" << std::flush;*/
}

bool GCodeService::canAbort()
{
	{
		std::lock_guard<std::mutex> guard(m_last_mut);
		if( (bool)m_last )
			return false;
	}
	{
		std::lock_guard<std::mutex> guard(m_active_mut);
		if( (bool)m_active )
			return false;
	}
	if(not rq.empty())
		return false;

	// std::cout << "can abort\n";
	return true;
}

bool GCodeService::abort(bool force)
{
	if(not canAbort() and not force)
		return false;

	{
		// abort m_last
		std::lock_guard<std::mutex> guard(m_last_mut);
		if((bool)m_last and force) {
			auto empty_command = std::make_shared<command::None>();
			cq.push(empty_command);
			m_last->abort();
			m_last.reset();
		}
	}

	{
		// abort m_active
		std::lock_guard<std::mutex> guard(m_active_mut);
		if((bool)m_active and force) {
			std::cout << "Need to abort m_active: " << m_active->getData() << "\n";
			m_active->abort();
			m_active.reset();

			auto empty_command = std::make_shared<command::None>();
			cq.push(empty_command);
		}
	}

	// clear all pending responses
	std::shared_ptr<command::Generic> cmd;
	while( rq.try_pop(cmd) ) {
		std::cout << "final-abort: " << cmd->getData() << std::endl;
		cmd->abort();
		cmd.reset();
	}

	// abort file pusher
	{
		std::lock_guard<std::mutex>  lock(m_file_status_mut);
	 	m_file_status = FileStatus::NONE;
	 	m_gfile.close();
	}

	// clear timeout list
	{
		std::lock_guard<std::mutex> guard(m_timeout_list_mut);
		m_timeout_list.remove_if([this](std::shared_ptr<command::Generic>& item){
			item->abort();
			publish(TOPIC_GCODE, {
				{GCODE_ASYNC_ABORTED}, {
					{"id", item->getId()}
				}
			});
			return true;
		});
	}

	return true;

}

void GCodeService::terminate()
{
	bool was_suspended = false;
	{
		std::lock_guard<std::mutex> guard(m_running_mut);
		was_suspended = (m_running_state == State::SUSPENDED);
		m_running_state = State::STOPPED;
	}

	if(was_suspended) {
		e_resume.set();
	}

	// abort any running command
	abort(true);
	// add an empty command to unlock the sender loop
	auto empty_command = std::make_shared<command::Terminate>();
	cq.push(empty_command);

	// put a dummy publication to unlock the publisher loop
	pq.clear();
	pq.push( {"", {}, {}} );

	m_serial.abort();
}

bool GCodeService::suspend(bool force)
{
	{
		std::lock_guard<std::mutex> guard(m_running_mut);
		if(m_running_state != State::RUNNING)
			return false;
	}

	if(not canAbort() and not force)
		return false;

	e_rx_state_changed.clear();
	e_tx_state_changed.clear();

	{
		std::lock_guard<std::mutex> guard(m_running_mut);
		e_resume.clear();
		m_running_state = State::SUSPENDED;
	}

	abort(force);

	auto empty_command = std::make_shared<command::None>();
	cq.push(empty_command);

	m_serial.abort();

	e_tx_state_changed.wait();
	e_rx_state_changed.wait();

	return true;
}

bool GCodeService::resume()
{
	{
		std::lock_guard<std::mutex> guard(m_running_mut);
		if(m_running_state != State::SUSPENDED)
			return false;
		m_running_state = State::RUNNING;
	}
	e_resume.set();

	e_rx_state_changed.wait();
	e_tx_state_changed.wait();

	e_rx_state_changed.clear();
	e_tx_state_changed.clear();

	return true;
}

bool GCodeService::serialOpen()
{
	//std::cout << "serialOpen: request\n";
	{
		std::lock_guard<std::mutex> guard(m_running_mut);
		if(m_running_state == State::STOPPED)
			return false;
	}
	if(m_serial.open()) {
		resume();
		return true;
	}
	return false;
}

bool GCodeService::serialClose(bool force)
{
	//std::cout << "serialClose: request\n";
	{
		std::lock_guard<std::mutex> guard(m_running_mut);
		if(m_running_state == State::STOPPED)
			return false;
	}
	if( suspend(force) ) {
		m_serial.close();
		return true;
	}

	return false;
}

void GCodeService::publisherThreadLoop() {

	while(true) {
		{
			std::lock_guard<std::mutex> guard(m_running_mut);
			if(m_running_state == State::STOPPED)
				return;
		}

		auto pub = pq.pop();
		if(not pub.topic.empty())
			m_session->publish(pub.topic, pub.args, pub.options);
			//publish_fn(pub.topic, pub.args, pub.options);
	}

}

void GCodeService::processReplyLine(const std::string& line)
{
	// std::cout << "<< " << line << std::endl;

	{
		std::lock_guard<std::mutex> guard(m_running_mut);
		if(m_running_state != State::RUNNING)
			return;
	}

	std::unique_lock<std::mutex> guard(m_active_mut);
	if(not (bool)m_active) {
		if( rq.try_pop(m_active) ) {

		}
	}

	// rx_count++;

	if((bool)m_active) {
		// std::cout << "m_active: " << m_active->getData() << std::endl;
		if( m_active->addReplyLine(line) ) {
			m_active->finish();
			if(m_active->isAsync()) {
				publish(TOPIC_GCODE, {
					{GCODE_ASYNC_FINISHED}, {
						{"id", m_active->getId()},
						{"line", line}
					}
				});
			}
			m_active.reset();
			guard.unlock();
		} else {
			if(m_active->isAsync()) {
				guard.unlock();
				publish(TOPIC_GCODE, {
					{GCODE_ASYNC_REPLY}, {
						{"id", m_active->getId()},
						{"line", line}
					}
				});
			}
		}
	} else {
		// Marlin out-of-order reply
		guard.unlock();
		//std::cout << "UNORDERED: " << line << std::endl;
		publish(TOPIC_GCODE, {
			{GCODE_ASYNC_UNORDERED}, {
				{"line", line}
			}
		});
	}
}

void GCodeService::receiverThreadLoop()
{
	//std::cout << "receiverThread started\n";
	e_rx_state_changed.set();

	bool running = true;
	std::string prev_fragment;
	while(true) {
		try {
			std::string data;

			{
				std::lock_guard<std::mutex>  lock(m_running_mut);
				if(m_running_state == State::STOPPED)
					return;
				running = (m_running_state == State::RUNNING);
			}

			if(running) {
				m_serial.readLine(data, 200);

				{
					std::lock_guard<std::mutex>  lock(m_running_mut);
					if(m_running_state == State::STOPPED)
						return;
					running = (m_running_state == State::RUNNING);
				}
			}

			if(!running) {
				//std::cout << "receiverThread: waiting\n" << std::flush;
				prev_fragment.clear();

				// Abort and clear all replies
				std::shared_ptr<command::Generic> dummy;
				while(rq.try_pop(dummy)) {
					dummy->abort();
				}
				dummy.reset();


				e_rx_state_changed.set();
				e_resume.wait();
				e_rx_state_changed.set();
				//std::cout << "receiverThread: resumed\n" << std::flush;
			} else {
				bool save_last = false;
				if( data.c_str()[data.length()-1] != '\n' ) {
					save_last = true;
				}

				// TODO: fix scenario ['ok','\nok\n'] => 'okok'
				auto lines = utils::str::split(data, '\n');
				unsigned len = lines.size();
				for(unsigned i=0; i<len; i++) {
					auto &line = lines[i];
					if(save_last and (i+1) == len) {
						prev_fragment = line;
					} else {
						if(!prev_fragment.empty()) {
							prev_fragment += line;
							processReplyLine(prev_fragment);
							prev_fragment.clear();
						} else {
							processReplyLine(line);
						}

					}
				}
			}

		} catch(...) {
			// @TODO: log error message
		}
	}
}

std::shared_ptr<command::Generic> GCodeService::sendGcode(const std::string& gcode)
{
	auto new_command = std::make_shared<command::GCode>(gcode);
	// TODO: split comment
	sendGcode(new_command);
	return new_command;
}

bool GCodeService::sendGcode(std::shared_ptr<command::Generic> cmd, const std::string& comment)
{
	std::string gcode = cmd->getData();
	GCode gcode_obj = m_parser.parse(gcode);
	bool skip = false;
	bool modify = not cmd->dontModify();

	if(modify) {
		skip = m_modifier.transform(gcode_obj, gcode, comment);
	}

	if(!skip) {
		// Note: if gcode is modifiable it means that it was not generated
		// and it should try to restore the corresponding overrider
		m_monitor.process(gcode_obj, comment, modify /* restore */);

		writeLine( cmd->getData() );
		// std::cout << ">> " << cmd->getData() << " rq:" << rq.size() <<  std::endl;
		rq.push(cmd);
		return true;
	}

	return false;
}

void GCodeService::pushLines()
{
	for(unsigned i=0; i<8; i++) {

		{
			std::lock_guard<std::mutex>  lock(m_running_mut);
			if(m_running_state != State::RUNNING)
				return;
		}

		FileStatus status;
		{
			std::lock_guard<std::mutex>  lock(m_file_status_mut);
		 	status = m_file_status;
		}

		if(status == FileStatus::PUSHING) {

			std::string gcode, comment;

			// check if there is a line in the modifier
			bool dont_modify = false;
			if(m_modifier.nextLine(gcode, comment)) {
				// there is a generated line
				dont_modify = true;
			} else {
				// there is nothing extra, get the next line from gcode file
				m_gfile.nextLine(gcode, comment);
			}

			if(not gcode.empty()) {

				//std::cout << "push:" << gcode << std::endl;
				{
					std::lock_guard<std::mutex> guard(m_last_mut);
					m_last = std::make_shared<command::GCode>(gcode);
					m_last->setDontModify(dont_modify);
					// check command
					sendGcode(m_last, comment);
				}

                // Goto wait status as those commands can take a while and
                // no push operation must be done before they are executed.
				if( (gcode.compare(0, 4, "M109") == 0) or (gcode.compare(0, 4, "M190") == 0) )   {
					std::lock_guard<std::mutex>  lock(m_file_status_mut);
					m_file_status =  FileStatus::WAIT;
				}

			}

			if(m_gfile.eof()) {
				// std::cout << "file: finished\n";
				std::lock_guard<std::mutex>  lock(m_file_status_mut);
				m_file_status = FileStatus::NONE;

				publish(TOPIC_GCODE_FILE, {
					// progress update notification
					{GCODE_FILE_PROGRESS}, {
						{"progress", 100.0f}
					}
				});

				bool have_last = false;
				{
					std::lock_guard<std::mutex> guard(m_last_mut);
					have_last = (bool)m_last;
				}

				if(have_last) {
					std::cout << "Waiting for command " << m_last->getData() << "\n";
					m_last->wait();

					std::cout << "Last: " << (m_last->isValid()?"OK\n":"Failed\n");

					std::lock_guard<std::mutex>  lock(m_last_mut);
					m_last.reset();
				}

				publish(TOPIC_GCODE_FILE, {
					{GCODE_FILE_FINISHED}, {}
				});
				m_gfile.close();

				return;
			}


		} else if( status == FileStatus::WAIT
				or status == FileStatus::PAUSED_WAIT ) {

			bool have_last = false;
			{
				std::lock_guard<std::mutex> guard(m_last_mut);
				have_last = (bool)m_last;
			}

			if(have_last) {
				// std::cout << "Waiting for command " << m_last->getData() << "\n";
				m_last->wait();

				if(!m_last->isValid()) {
					// std::cout << "waiting aborted\n";
					return;
				}

				{
					std::lock_guard<std::mutex>  lock(m_last_mut);
					m_last.reset();
				}

				// make sure we capture the ABORTING status
				{
					std::lock_guard<std::mutex>  lock(m_file_status_mut);
				 	status = m_file_status;
				}

				if(status == FileStatus::WAIT) {
					std::lock_guard<std::mutex>  lock(m_file_status_mut);
					m_file_status =  FileStatus::PUSHING;
				} else if (status == FileStatus::PAUSED_WAIT) {
					std::lock_guard<std::mutex>  lock(m_file_status_mut);
					m_file_status =  FileStatus::PAUSED;
					// paused notification
					publish(TOPIC_GCODE_FILE, {
						{GCODE_FILE_PAUSED}, {}
					});
					return;
				} else if (status == FileStatus::ABORTING
					|| status == FileStatus::NONE )
				{
					return;
				}
			}
		} else if( status == FileStatus::ABORTING ) {
			return;
		}

		// check progress
		float min_progress;
		{
			std::lock_guard<std::mutex>  guard(m_file_notify_mut);
			min_progress = m_file_notify_settings.min_progress;
		}
		float new_progress = m_gfile.getProgress();
		if( fabs(new_progress - m_old_progress) >= min_progress ) {
			m_old_progress = new_progress;
			publish(TOPIC_GCODE_FILE, {
				// progress update notification
				{GCODE_FILE_PROGRESS}, {
					{"progress", new_progress}
				}
			});
		}
	}

}

void GCodeService::senderThreadLoop()
{
	// std::cout << "senderThread started\n";
	e_tx_state_changed.set();

	while(true) {
		std::shared_ptr<command::Generic> cmd;

		FileStatus state;
		{
			std::lock_guard<std::mutex>  lock(m_file_status_mut);
		 	state = m_file_status;
		}

		// std::cout << "FILE_STATUS: " << (int)state << std::endl;

		if(	state != FileStatus::NONE and
			state != FileStatus::LOADED and
			state != FileStatus::ABORTING) {
			if( cq.try_pop(cmd) ) {
				//
				// std::cout << "pop: OK\n";
			} else {
				// push lines
				pushLines();
				continue;
			}
		} else {
			cmd = cq.pop();
		}

		bool running = true;
		{
			std::lock_guard<std::mutex>  lock(m_running_mut);
			if(m_running_state == State::STOPPED)
				return;
			running = (m_running_state == State::RUNNING);
		}

		if(!running) {

			if(cmd) {
				cmd->abort();
			}

			// Abort and clear all commands
			std::shared_ptr<command::Generic> dummy;
			while(cq.try_pop(dummy)) {
				dummy->abort();
			}
			dummy.reset();

			e_tx_state_changed.set();
			e_resume.wait();
			e_tx_state_changed.set();
			// std::cout << "senderThread: resumed\n" << std::flush;
		} else {
			switch(cmd->getType()) {
				case command::Type::TERMINATE:
					{
						break;
					}

				case command::Type::GCODE:
					{
						sendGcode(cmd);
						break;
					}

				case command::Type::FILE_LOAD:
					{
						auto fileName = cmd->getData();
						// std::cout << "request:file_load( " << fileName << " )\n" << std::flush;
						if( not m_gfile.isOpened() and m_gfile.open(fileName) ) {

							m_old_progress = 0.0f;

							bool loaded = false;
							{
								std::lock_guard<std::mutex>  lock(m_file_status_mut);
								if(m_file_status == FileStatus::NONE) {
							 		m_file_status = FileStatus::LOADED;
							 		loaded = true;
								}
							}
							if(loaded) {
								cmd->finish();
								// std::cout << "success:file_load\n" << std::flush;
							} else {
								// std::cout << "failed:file_load #1\n" << std::flush;
								cmd->abort();
							}
						} else {
							// std::cout << "failed:file_load #2\n" << std::flush;
							cmd->abort();
						}
						break;
					}

				case command::Type::FILE_PUSH:
					{
						// std::cout << "request:file_push\n" << std::flush;

						bool started = false;
						{
							std::lock_guard<std::mutex>  guard(m_file_status_mut);
							if(m_file_status == FileStatus::LOADED) {
								m_file_status = FileStatus::PUSHING;
								started = true;
							}
						}

						if(started) {
							publish(TOPIC_GCODE_FILE, {
								{GCODE_FILE_STARTED}, {}
							});

							cmd->finish();
							// std::cout << "success:file_push\n" << std::flush;
							// started notification
						} else {
							// std::cout << "failed:file_push #1\n" << std::flush;
							publish(TOPIC_GCODE_FILE, {
								{GCODE_FILE_ABORTED}, {}
							});

							cmd->abort();
						}
						break;
					}

				case command::Type::FILE_PAUSE:
					{
						std::cout << "request:file_pause\n" << std::flush;
						// TODO:
						{
							std::lock_guard<std::mutex>  guard(m_file_status_mut);
							if(m_file_status == FileStatus::PUSHING){
								m_file_status = FileStatus::PAUSED;
								// paused notification
								publish(TOPIC_GCODE_FILE, {
									{GCODE_FILE_PAUSED}, {}
								});
							} else if(m_file_status == FileStatus::WAIT) {
								m_file_status = FileStatus::PAUSED_WAIT;
							}
						}
						cmd->finish();
						break;
					}

				case command::Type::FILE_RESUME:
					{
						std::cout << "request:file_resume\n" << std::flush;
						// TODO:
						{
							std::lock_guard<std::mutex>  guard(m_file_status_mut);
							if(m_file_status == FileStatus::PAUSED){
								m_file_status = FileStatus::PUSHING;
								// resumed notification
								publish(TOPIC_GCODE_FILE, {
									{GCODE_FILE_RESUMED}, {}
								});
							} else if(m_file_status == FileStatus::PAUSED_WAIT) {
								m_file_status = FileStatus::WAIT;
								publish(TOPIC_GCODE_FILE, {
									{GCODE_FILE_RESUMED}, {}
								});
							}
						}
						cmd->finish();
						break;
					}

				case command::Type::FILE_ABORT:
					{
						// std::cout << "file_abort request\n" << std::flush;
						{
							std::lock_guard<std::mutex> guard(m_last_mut);
							if((bool)m_last) {
								m_last->wait();
								m_last.reset();
							}
						}

						m_gfile.close();

						publish(TOPIC_GCODE_FILE, {
							{GCODE_FILE_ABORTED}, {}
						});

						{
							std::unique_lock<std::mutex>  guard(m_file_status_mut);
							m_file_status = FileStatus::NONE;
						}

						cmd->finish();

						break;
					}
				default:;
			} // switch
		}
	} // while

	// std::cout << "senderThread finished\n";
}

void GCodeService::writeLine(const std::string& line)
{
	// tx_count++;

	m_serial.write(line);
	m_serial.writeByte('\n');
}

std::shared_ptr<command::Generic> GCodeService::send(const std::string &code, const std::string &reply, bool use_regex, unsigned timeout)
{
	auto new_command = std::make_shared<command::GCode>(code, reply, use_regex, timeout);
	cq.push(new_command);

	new_command->wait();
	return new_command;
}

unsigned GCodeService::sendAsync(const std::string &code, const std::string &reply, bool use_regex, unsigned timeout)
{
	auto new_command = std::make_shared<command::GCodeAsync>(code, reply, use_regex, timeout);
	unsigned id;
	{
		std::lock_guard<std::mutex> guard(m_async_mut);
		id = ++m_async_id;
	}
	new_command->setId(id);
	cq.push(new_command);

	{
		std::lock_guard<std::mutex> guard(m_timeout_list_mut);
		m_timeout_list.push_front(new_command);
	}

	publish(TOPIC_GCODE, {
		{GCODE_ASYNC_STARTED}, {
			{"id", id},
			{"command", code}
		}
	});

	return id;
}

bool GCodeService::abortAsync(unsigned id)
{
	std::lock_guard<std::mutex>  lock(m_timeout_list_mut);
	bool result = false;
	m_timeout_list.remove_if([this, &id, &result](std::shared_ptr<command::Generic>& item){
		if(item->getId() == id) {
			publish(TOPIC_GCODE, {
				{GCODE_ASYNC_ABORTED}, {
					{"id", item->getId()}
				}
			});
			result = true;
			return true;
		}
		return false;
	});
	return result;
}

void GCodeService::timeoutThreadLoop()
{
	auto running_state = State::STOPPED;
	while(true) {
		{
			std::lock_guard<std::mutex>  lock(m_running_mut);
			running_state = m_running_state;
			if(running_state == State::STOPPED)
				return;
		}

		{
			std::lock_guard<std::mutex>  lock(m_timeout_list_mut);
			m_timeout_list.remove_if([this](std::shared_ptr<command::Generic>& item){
				if(item->checkExpired()) {
					publish(TOPIC_GCODE, {
						{GCODE_ASYNC_EXPIRED}, {
							{"id", item->getId()}
						}
					});
					return true;
				}
				return false;
			});
		}

		std::this_thread::sleep_for( std::chrono::milliseconds(100) );
	}
}

bool GCodeService::fileLoad(const std::string& fileName)
{
	{
		std::lock_guard<std::mutex> guard(m_running_mut);
		if(m_running_state != State::RUNNING)
			return false;
	}
	auto file_load = std::make_shared<command::FileLoad>(fileName);
	cq.push(file_load);

	file_load->wait();
	return file_load->isValid();
}

bool GCodeService::filePush()
{
	{
		std::lock_guard<std::mutex> guard(m_running_mut);
		if(m_running_state != State::RUNNING)
			return false;
	}
	auto file_push = std::make_shared<command::FilePush>();
	cq.push(file_push);

	file_push->wait();
	return file_push->isValid();
}

bool GCodeService::fileLoadAndPush(const std::string& fileName)
{
	{
		std::lock_guard<std::mutex> guard(m_running_mut);
		if(m_running_state != State::RUNNING)
			return false;
	}
	auto file_load = std::make_shared<command::FileLoad>(fileName);
	cq.push(file_load);
	file_load->wait();

	if(not (bool)file_load)
		return false;

	auto file_push = std::make_shared<command::FilePush>();
	cq.push(file_push);

	file_push->wait();
	return file_push->isValid();
}

bool GCodeService::filePause()
{
	{
		std::lock_guard<std::mutex> guard(m_running_mut);
		if(m_running_state != State::RUNNING)
			return false;
	}

	{
		std::lock_guard<std::mutex>  lock(m_file_status_mut);
		if(m_file_status != FileStatus::PUSHING and m_file_status != FileStatus::WAIT)
			return false;
	}

	auto file_pause = std::make_shared<command::FilePause>();
	cq.push(file_pause);
	file_pause->wait();
	return true;
}

bool GCodeService::fileResume()
{
	{
		std::lock_guard<std::mutex> guard(m_running_mut);
		if(m_running_state != State::RUNNING)
			return false;
	}

	{
		std::lock_guard<std::mutex>  lock(m_file_status_mut);
		// std::cout << "resume_request on " << (int)m_file_status << std::endl;
		if(m_file_status != FileStatus::PAUSED and m_file_status != FileStatus::PAUSED_WAIT)
			return false;
	}

	auto file_resume = std::make_shared<command::FileResume>();
	cq.push(file_resume);
	file_resume->wait();
	return true;
}

bool GCodeService::fileAbort()
{
	{
		std::lock_guard<std::mutex> guard(m_running_mut);
		if(m_running_state != State::RUNNING) {
			return false;
		}
	}

	FileStatus status;
	{
		std::lock_guard<std::mutex>  lock(m_file_status_mut);
	 	status = m_file_status;

	 	if( status != FileStatus::PUSHING and
	 		status != FileStatus::PAUSED and
	 		status != FileStatus::PAUSED_WAIT and
	 		status != FileStatus::ABORTING) {
	 		return false;
	 	}
	}

	bool hard_reset = false;
	if((status == FileStatus::WAIT ||
		status == FileStatus::PAUSED_WAIT))
	{
		/* Pusher is waiting for a long run command (M190, M109...)
		 * we must do a controller reset to ensure the command is aborted */
		std::lock_guard<std::mutex> guard(m_file_status_mut);
		m_file_status = FileStatus::ABORTING;
		hard_reset = true;
	}

	// This is the second time fileAbort is being called
	// so we will do a hard_reset to ensure the operations
	// are aborted now
	if(status == FileStatus::ABORTING) {
		hard_reset = true;
	}

	if(hard_reset) {
		// std::cout << "Will need to do a hard-reset\n";
		auto cfut = m_session->call("controller.reset");
		cfut.wait();

		abort(true);

		/*{
			std::lock_guard<std::mutex> guard(m_file_status_mut);
			m_file_status = FileStatus::NONE;
		}*/

		publish(TOPIC_GCODE_FILE, {
			{GCODE_FILE_ABORTED}, {}
		});

		return true;
	}

	auto file_abort = std::make_shared<command::FileAbort>();
	cq.push(file_abort);
	file_abort->wait();

	return true;
}

bool GCodeService::fileSetOverride(const std::string& override, float value, unsigned index)
{
	bool result = false;
	result = m_modifier.setOverride(override, value, index);
	return result;
}

bool GCodeService::fileSetNotifyParams(float min_progress, bool status_update, bool height_update)
{
	std::lock_guard<std::mutex>  guard(m_file_notify_mut);
	m_file_notify_settings.min_progress = min_progress;
	m_file_notify_settings.status_update = status_update;
	m_file_notify_settings.height_update = height_update;
	return true;
}

// fileGetOverride

wampcc::json_object GCodeService::fileInfo()
{
	FileStatus file_status;
	{
		std::lock_guard<std::mutex> guard(m_file_status_mut);
		file_status = m_file_status;
	}

	std::string status;
	switch(file_status) {
		case FileStatus::LOADED:
			status = "LOADED";
			break;
		case FileStatus::PUSHING:
			status = "PUSHING";
			break;
		case FileStatus::WAIT:
			status = "WAIT";
			break;
		case FileStatus::PAUSING:
			status = "PAUSING";
			break;
		case FileStatus::PAUSED:
			status = "PAUSED";
			break;
		case FileStatus::PAUSED_WAIT:
			status = "PAUSED_WAIT";
			break;
		case FileStatus::RESUMING:
			status = "RESUMING";
			break;
		case FileStatus::ABORTING:
			status = "ABORTING";
			break;
		/*case FileStatus::DONE:
			status = "DONE";
			break;*/
		default:
			status = "NONE";
	}

	unsigned number_of_lines = m_gfile.getNumberOfLines();
	unsigned current_line = m_gfile.getCurrentLineNumber();

	return {
		{"status", status },
		{"number_of_lines", number_of_lines},
		{"current_line", current_line},
		{"override", m_modifier.info() }
	};
}

wampcc::json_object GCodeService::serialInfo()
{
	return {
		{"port_name", m_serial.portName() },
		{"baud_rate", m_serial.baudRate() },
		{"opened", m_serial.isOpen() }
	};
}

wampcc::json_object GCodeService::info()
{
	State state;
	{
		std::lock_guard<std::mutex> guard(m_running_mut);
		state = m_running_state;
	}

	auto file_info = fileInfo();
	auto serial_info = serialInfo();

	std::string status;

	switch(state) {
		case State::STOPPED:
			status = "STOPPED";
			break;
		case State::RUNNING:
			status = "RUNNING";
			break;
		case State::SUSPENDED:
			status = "SUSPENDED";
			break;
	}

	return {
		{"status", status},
		{"file", file_info},
		{"serial", serial_info}
	};
}
