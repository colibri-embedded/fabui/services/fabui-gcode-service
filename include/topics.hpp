#ifndef TOPICS_HPP
#define TOPICS_HPP

#define TOPIC_GCODE 		"gcode.events"
#define TOPIC_GCODE_FILE	"gcode.file.events"

#define GCODE_ASYNC_STARTED		"async.started"
#define GCODE_ASYNC_FINISHED	"async.finished"
#define GCODE_ASYNC_ABORTED		"async.aborted"
#define GCODE_ASYNC_EXPIRED		"async.expired"
#define GCODE_ASYNC_REPLY		"async.reply"
#define GCODE_ASYNC_UNORDERED	"async.unordered"

#define GCODE_FILE_LOADED		"file.loaded"
#define GCODE_FILE_STARTED		"file.started"
#define GCODE_FILE_ABORTED		"file.aborted"
#define GCODE_FILE_PAUSED		"file.paused"
#define GCODE_FILE_RESUMED		"file.resumed"
#define GCODE_FILE_FINISHED		"file.finished"
#define GCODE_FILE_PROGRESS		"file.progress"

#endif /* TOPICS_HPP */

