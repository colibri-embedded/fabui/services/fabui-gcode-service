/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: gcode_file.hpp
 * @brief GCode file definition
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#ifndef GCODEFILE_HPP
#define GCODEFILE_HPP

#include <string>
#include <mutex>
#include <iostream>
#include <fstream>

namespace fabui {

/// GCode file loader.
/**
 * GCodeFile class loads a gcode file and allows thread safe access to it by 
 * reading the content line by line and splitting it in gcode command and comment.
 */
class GCodeFile {
	public:
		/// Create a GCodeFile
		GCodeFile();
		/// Create a GCodeFile and load the content from fileName
		GCodeFile(const std::string& fileName);

		~GCodeFile();

		/**
		 * Close file handler
		 */
		void close();

		/** 
		 * Load content from fileName. If the fileName is already provided
		 * the file is not loaded yet it will be loaded.
		 * 
		 * @param fileName File path to a gcode file
		 * @returns true on success, false otherwise
		 */
		bool open(const std::string& fileName="");

		/// Get current fileName
		std::string getFileName();

		/**
		 * Check if the file is loaded
		 * 
		 * @returns true if file is loaded, false otherwise
		 */
		bool isOpened();

		/** 
		 * Check if we are at the EOF
		 * 
		 * @returns File handler is at EOF
		 */
		bool eof();

		/**
		 * Read the next line in the loaded file. Extract gcode command and comment from the next line.
		 *
		 * @param gcode GCode command string output variable
		 * @param comment GCode comment string output variable
		 * @param no_guard Don't use mutex guards if false
		 * 
		 * @returns true on success, false otherwise
		 */
		bool nextLine(std::string& gcode, std::string& comment, bool no_guard=false);

		/// Get the total number of lines
		unsigned getNumberOfLines();
		
		/// Get current line number
		unsigned getCurrentLineNumber();

		/// Get the current file progress based on total lines an current line
		float getProgress();
	private:
		/// GCode filename
		std::string m_filename;
		/// GCode file stream object
		std::ifstream file;
		// Total number of lines
		unsigned m_total_lines;
		// Current line number
		unsigned m_current_line;
		// Mutex
		std::mutex m_shared_mut;
		
		/// Process file content after loading
		bool processFile();
		/// Reser all values to defaults
		void reset();
};

} // namespace fabui

#endif /* GCODEFILE_HPP */ 