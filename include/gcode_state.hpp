/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: gcode_service.hpp
 * @brief GCode WAMP service definition
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#ifndef GCODE_STATE_HPP
#define GCODE_STATE_HPP

#include <cstdint>
#include "gcode_types.hpp"

namespace fabui {

struct GCodeState {
	/* Machine */
	float speed;
	float flow;

	/* Positioning mode*/
	gcode::Positioning xyz_mode;
	gcode::Positioning e_mode;

	/* Feedrates */
	unsigned feedrate;

	/* Fan */
	uint8_t fan_pwm;

	/* Spindle */
	unsigned rpm;
	gcode::Rotation rotation;

	/* Position */
	float z;

	/* Temperature */
	float bed_target;
	float nozzle_target[MAX_NOZZLE_COUNT];

	///
	GCodeState();

	///
	void reset();

};

} // namespace fabui

#endif /* GCODE_STATE_HPP */