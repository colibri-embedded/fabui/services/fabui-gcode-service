/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: command.hpp
 * @brief GCodeService command definition
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#ifndef COMMAND_HPP
#define COMMAND_HPP

#include <string>
#include <vector>
#include <mutex>
#include <regex>
#include <chrono>
#include <wampcc/wampcc.h>
#include <fabui/thread/event.hpp>

namespace fabui {
namespace command {

/// GCodeService command type id
enum class Type {
	NONE = 0,
	TERMINATE,
	/* GCode */
	GCODE,
	/* File */
	FILE_LOAD,
	FILE_PUSH,
	FILE_PAUSE,
	FILE_RESUME,
	FILE_ABORT
};

/// GCodeService Generic command
class Generic {
	protected:
		Type m_type;
		/* State */
		bool m_dont_modify;
		bool m_aborted;
		bool m_expired;
		bool m_async;
		unsigned m_id;
		/* GCode specific */
		std::string m_command;
		std::string m_expected_reply;
		std::regex  m_regex;
		bool		m_use_regex;
		std::vector<std::string> m_reply;
		/* Events */
		std::mutex m_mutex;
		Event done;
		/* Timeout */
		std::chrono::system_clock::time_point m_expires_at;

		enum class State {
			WAITING,	// 0
			FINISHED,	// 1
			ABORTED,	// 2
			EXPIRED 	// 3
		};

		State m_state;
	public:
		/// Create Generic command
		Generic(Type type, const std::string& command,
			const std::string &expected_reply = "ok",
			bool use_regex=false,
			unsigned timeout=1000,
			bool async=false);

		std::string getData();
		std::vector<std::string> getReply();

		void setId(unsigned);
		unsigned getId();

		void setDontModify(bool);
		bool dontModify();

		Type getType();
		bool isAborted();
		bool isExpired();
		bool checkExpired();
		bool isValid();

		bool isAsync();

		void finish();
		void abort();
		void expire();
		bool wait();

		bool addReplyLine(const std::string& line);

		operator bool();

		wampcc::json_array toArray();
		wampcc::json_object toObject();
};

/// GCodeService Empty/Dummy command
class None: public Generic {
	public:
		/// Create an Empty command
		None();
};

/// GCodeService Terminate command
class Terminate: public Generic {
	public:
		/// Create a Terminate command
		Terminate();
};

/// GCodeService GCode command
class GCode: public Generic {
	public:
		/// Create a GCode command
	GCode(
		const std::string& gcode,
		const std::string &expected_reply="ok",
		bool use_regex=false,
		unsigned timeout=1000);
};

/// GCodeService asynchronous gcode command
class GCodeAsync: public Generic {
	public:
		/// Create an asynchronous GCode command
	GCodeAsync(
		const std::string& gcode,
		const std::string &expected_reply="ok",
		bool use_regex=false,
		unsigned timeout=1000);
};

/// GCodeService Load file command
class FileLoad: public Generic {
	public:
		/// Create a Load file command
		FileLoad(const std::string& fileName, unsigned timeout=10000);

		/// Get filename
		std::string getFileName();
};

/// GCodeService Push file command
class FilePush: public Generic {
	public:
		/// Create a Push file command
		FilePush(unsigned timeout=1000);
};

/// GCodeService Pause file command
class FilePause: public Generic {
	public:
		/// Create a Pause file command
		FilePause(unsigned timeout=30000);
};

/// GCodeService Resume file command
class FileResume: public Generic {
	public:
		/// Create a Resume file command
		FileResume(unsigned timeout=30000);
};

/// GCodeService Abort file command
class FileAbort: public Generic {
	public:
		/// Create a Abort file command
		FileAbort(unsigned timeout=30000);
};

} // namespace command
} // namespace fabui

#endif /* COMMAND_HPP */
