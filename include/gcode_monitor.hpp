/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: gcode_monitor.hpp
 * @brief GCode monitor definition
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#ifndef GCODE_MONITOR_HPP
#define GCODE_MONITOR_HPP

#include <string>
#include "gcode_parser.hpp"
#include "gcode_state.hpp"

namespace fabui {

class GCodeModifier;

class GCodeMonitor {
	public:
		/// Create a GCodeMonitor
		GCodeMonitor(GCodeState &state, GCodeModifier& modifier);

		/**
		 * Process incoming gcode and update the global state...TODO publish events
		 */
		void process(GCode& gcode_obj, const std::string& comment, bool restore=false);
	private:
		/// Global GCode state object
		GCodeState& m_state;
		/// GCode modifier object
		GCodeModifier& m_modifier;
};

} // namespace fabui

#endif /* GCODE_MONITOR_HPP */