#ifndef PLUGIN_GCODE_HPP
#define PLUGIN_GCODE_HPP

#include <string>
#include <vector>
#include <dlfcn.h>
#include <map>
#include <memory>

#include <command.hpp>
#include <wampcc/wampcc.h>
#include <fabui/wamp/session.hpp>

namespace plugin {
	namespace gcode {

		class Plugin {
			public:
				Plugin(fabui::WampSession* session);

				virtual std::string name() =0;
				virtual std::string description() =0;

				virtual void parseAsyncLine(const std::string& line) =0;
				virtual wampcc::json_object parseReplyLine(fabui::command::GCode* command, const std::string& line) =0;

				//virtual std::map<std::string, std::string>  parseM503(std::vector<std::string>& lines) =0;
			protected:
				fabui::WampSession* m_session;
		};

		typedef Plugin* (*create_fn_t)(fabui::WampSession *session);

	} // namespace gcode
} // namespace plugin

#endif /* PLUGIN_GCODE_HPP */

