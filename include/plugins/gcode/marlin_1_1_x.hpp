#ifndef PLUGIN_GCODE_MARLIN_1_1_X_HPP
#define PLUGIN_GCODE_MARLIN_1_1_X_HPP

#include <regex>
#include <plugins/gcode/base.hpp>

namespace plugin {
	namespace gcode {
		class Marlin_1_1_X: public Plugin {
			public:
				Marlin_1_1_X(fabui::WampSession* session);

				std::string name();
				std::string description();

				void parseAsyncLine(const std::string& line);
				wampcc::json_object parseReplyLine(fabui::command::GCode* command, const std::string& line);

				//std::map<std::string, std::string>  parseM503(std::vector<std::string>& lines);
			private:
				std::regex m_m190_regex;
				std::regex m_m109_regex;
				std::regex m_m105_regex;
				std::regex m_m303_regex;
				std::regex m_m114_regex;
		};
	}
}

#endif /* PLUGIN_GCODE_MARLIN_1_1_X_HPP */
