#ifndef SERVICE_TYPES_HPP
#define SERVICE_TYPES_HPP

#define MAX_NOZZLE_COUNT 	5
#define MAX_PWM_VALUE		255

namespace fabui {

enum class OverrideType : int {
	Z,
	SPEED,
	FLOW,
	RPM,
	PWM,
	FAN,
	NOZZLE_TEMP,
	BED_TEMP,
	ALL
};

union OverrideTypeUnion
{
    OverrideType e;
    int flag;
};

enum class FileStatus {
	NONE,		// 0
	LOADED,		// 1
	PUSHING,	// 2
	WAIT,		// 3
	PAUSING,	// 4
	PAUSED,		// 5
	PAUSED_WAIT,// 6
	RESUMING,	// 7
	ABORTING,	// 8
	//DONE		// 9
};

struct FileNotifySettings {
	float min_progress;
	bool  status_update;
	bool  height_update;

	FileNotifySettings()
		: min_progress(0.1f)
		, status_update(true)
		, height_update(true)
		{}
};

namespace gcode {

	enum class Positioning {
		ABSOLUTE,
		RELATIVE
	};

	enum class Rotation {
		CW,
		CCW
	};

} // namespace gcode
} // namespace fabui

#endif /* SERVICE_TYPES_HPP */