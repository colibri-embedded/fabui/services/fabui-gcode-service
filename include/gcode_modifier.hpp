/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: gcode_modifier.hpp
 * @brief GCode modifier definition
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#ifndef GCODE_MODIFIER_HPP
#define GCODE_MODIFIER_HPP

#include <forward_list>
#include <functional>
#include <string>
#include <memory>
#include <tuple>
#include <wampcc/wampcc.h>
#include <fabui/thread/queue.hpp>

#include "gcode.hpp"
#include "gcode_state.hpp"

namespace fabui {

struct GCodeOverride {
	std::string name;
	unsigned index;
	float value;

	bool active;
	bool locked;

	std::function<bool(GCodeOverride*, GCode&, std::string&, bool&)> transform;
	std::function<void(GCodeOverride*, float,float)> generate;
	std::function<void(GCodeOverride*)> reset;
};

class GCodeModifier {
	public:
		/// Create a GCodeModifier
		GCodeModifier(GCodeState &state);

		/**
		 * Return a generated gcode line.
		 *
		 * @param gcode GCode command output variable
		 * @param command GCode comment output variable
		 *
		 * @returns true if there is a line, false otherwise
		 */
		bool nextLine(std::string& gcode, std::string& comment);

		/**
		 * Push a gcode line to the gcode generated list.
		 *
		 * @param line GCode line
		 */
		void pushLine(const std::string& line);

		/**
		 * Transform a gcode command if needed.
		 *
		 * @param gcode Gcode line to be modified
		 * @param gcode Gcode comment to me modified
		 *
		 * @returns true if transform check should continue, false if not
		 */
		bool transform(GCode& gcode_obj, std::string& gcode, const std::string& comment);

		/**
		 * Reset override value. Switches off the modification of indicated value.
		 *
		 * @param type Override type to reset.
		 */
		void reset(const std::string& name, unsigned index = 0);

		/**
		 * Deactivate/restore override value if it is not locked.
		 *
		 * @param type Override type to reset.
		 */
		void restore(const std::string& name, unsigned index = 0);

		/**
		 * Lock and override type value. Once the value is locked every automatic change
		 * to this value is ignored. No new values from the gcode file are used.
		 *
		 * @param type Override type
		 * @param locked Locked state
		 */
		void setLocked(const std::string& name, bool locked=true, unsigned index = 0);

		/**
		 * Set override value for a particular override type.
		 *
		 * @param name Override type name
		 * @param value New override value
		 * @param index Value index in case of multi-value (ex.: nozzle temperature)
		 * @returns true if value is different from existing value, false otherwise
		 */
		bool setOverride(const std::string& name, float value, unsigned index = 0);

		/**
		 * Get override settings for particular override type.
		 *
		 * @param name Override type name
		 * @param index Value index in case of multi-value (ex.: nozzle temperature)
		 *
		 * @returns tuple(active, locked, value)
		 */
		std::tuple<bool, bool, float> getOverride(const std::string& name, unsigned index = 0);

		/**
		 * ...
		 */
		wampcc::json_object info();

	private:
		/// Modifiers list
		std::forward_list< std::unique_ptr<GCodeOverride> > modifiers;
		/// Extra commands queue
		Queue<std::string, 0> m_lines;
		/// Gcode state reference
		GCodeState& m_state;

		/**
		 * Add modifier object to modifier list.
		 *
		 * @param name Overrider type name
		 * @param index index Value index in case of multi-value (ex.: nozzle temperature)
		 * @param value Default value
		 * @returns New override object
		 */
		GCodeOverride* addModifier(const std::string& name, unsigned index=0, float value=0.0f);
};

} // namespace fabui

#endif /* GCODE_MODIFIER_HPP */
